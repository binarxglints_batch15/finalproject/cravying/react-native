import React from 'react';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import Appstack from './src/navigasi/Appstack';
import {Provider} from 'react-redux';
import storeRedux from './src/redux/store';
const App = () => {
  return (
    <Provider store={storeRedux}>
      <NavigationContainer>
        <Appstack />
      </NavigationContainer>
    </Provider>
  );
};

export default App;
