const initialState = {
  isLoading: false,
  dataMenu: [],
};

const menucat = (state = initialState, action) => {
  switch (action.type) {
    case 'GETDATA_START':
      return {
        ...state,
        isLoading: true,
      };
    case 'GETDATA_SUCCESS':
      return {
        ...state,
        isLoading: false,
        dataMenu: action.data,
      };
    case 'GETDATA_FAILED':
      console.log(action);
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
};

export default menucat;
