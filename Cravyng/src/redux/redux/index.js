import {combineReducers} from 'redux';
import auth from './auth';
import signup from './signup';
import menucat from './menucat';
export default combineReducers({
  auth,
  signup,
  menucat,
});
