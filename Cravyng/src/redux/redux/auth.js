const initialState = {
  isLoading: false,
  dataLogin: '',
  error: '',
};

const auth = (state = initialState, action) => {
  switch (action.type) {
    case 'LOGIN_START':
      return {
        ...state,
        isLoading: true,
        token: '',
        role: '',
      };
    case 'LOGIN_SUCCESS':
      return {
        ...state,
        isLoading: false,
      };
    case 'LOGIN_FAILED':
      console.log(action);
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    default:
      return state;
  }
};

export default auth;
