const initialState = {
  isLoadingSignup: false,
};

const signup = (state = initialState, action) => {
  switch (action.type) {
    case 'SIGNUP_START':
      return {
        ...state,
        isLoadingSignup: true,
      };
    case 'SIGNUP_SUCCES':
      return {
        ...state,
        isLoadingSignup: false,
      };
    case 'SINGUP_FAILED':
      return {
        ...state,
        isLoadingSignup: true,
      };
    default:
      return state;
  }
};

export default signup;
