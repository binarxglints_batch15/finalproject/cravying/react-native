import {takeLatest, put} from '@redux-saga/core/effects';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Alert} from 'react-native';

function* signup(action) {
  try {
    console.log('TRY');
    console.log('data yang di passing ke reduxsaga', action.data);
    const resRegister = yield axios({
      method: 'POST',
      url: 'https://cravyng.herokuapp.com/user/signup/',
      data: action.data,
    });

    console.log('INI DATA DARI BE SIGN UP', resRegister.data);
    if (resRegister && resRegister.data) {
      yield put({
        type: 'SIGNUP_SUCCESS',
      });
      alert('REGISTER BERHASIL SILAHKAN LOGIN');
      action.navigasi.navigate('Signin');
    }
  } catch (error) {
    console.log(error);
    yield put({type: 'SIGNUP_FAILED'});
    alert('TRY AGAIN LATER');
  }
}

function* rootSaga() {
  yield takeLatest('SIGNUP_START', signup);
}

export default rootSaga;
