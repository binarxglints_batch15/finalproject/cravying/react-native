import {takeLatest, put} from 'redux-saga/effects';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

const storeToken = async value => {
  try {
    await AsyncStorage.setItem('TOKEN', value);
    console.log('Penyimpanan token berhasil');
  } catch (e) {
    // saving error
  }
};

const storeRole = async value => {
  try {
    await AsyncStorage.setItem('ROLE', value);
    console.log('Penyimpanan role berhasil');
  } catch (e) {
    // saving error
  }
};

function* Login(action) {
  try {
    console.log('LOGIN_START');
    const resLogin = yield axios({
      method: 'POST',
      url: 'https://cravyng.herokuapp.com/user/signin/',
      data: action.data,
    });
    console.log(action.data);
    if (resLogin && resLogin.data) {
      yield storeToken(resLogin.data.token);
      yield storeRole(resLogin.data.role);
      yield put({
        type: 'LOGIN_SUCCESS',
      });
      action.navigasi.navigate('Tabs');
    }
  } catch (err) {
    console.log(err);
    yield put({
      type: 'LOGIN_FAILED',
      error: err.response.data.message,
    });

    console.log(err.response.data.message);
  }
}

function* rootSaga() {
  yield takeLatest('LOGIN_START', Login);
}

export default rootSaga;
