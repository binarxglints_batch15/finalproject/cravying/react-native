import {all} from 'redux-saga/effects';
import auth from './auth';
import signup from './signup';
import menucat from './menucat';
export default function* rootSaga() {
  yield all([auth(), signup(), menucat()]);
}
