import {takeLatest, put} from '@redux-saga/core/effects';
import axios from 'axios';

function* getData(action) {
  try {
    console.log('try');
    const resGetMenu = yield axios({
      method: 'GET',
      url: 'https://cravyng.herokuapp.com/menu/home',
    });
    yield put({
      type: 'GETDATA_SUCCESS',
      data: resGetMenu.data,
    });
    console.log('tes masuk');
  } catch (err) {
    console.log('EROR GET EVENT FROM API:', err);
  }
}

function* rootSaga() {
  yield takeLatest('GETDATA_START', getData);
}

export default rootSaga;
