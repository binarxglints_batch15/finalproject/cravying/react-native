import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import SplashScreen from '../screens/SplashScreen';
import SplashScreen2 from '../screens/SplashScreen2';
import Signin from '../screens/Signin';
import MenuDetail from '../screens/Menu/MenuDetail';
import MerchantProfile from '../screens/Merchant/MerchantProfile';
import ESProfile from '../screens/ESProfile';
import Login from '../screens/Login';
import Signin3 from '../screens/Signin3';
import Signup from '../screens/Signup';
import Signup2 from '../screens/Signup2';
import Checkout1 from '../screens/Checkout1merchant';
import Profile2 from '../screens/Profile2';
import Cart from '../screens/Cart/Cart1b';
import Checkout2 from '../screens/Checkout2';
import MyTabs from '../navigasi/MyTabs';
const Stack = createStackNavigator();
const Appstack = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Splash" component={SplashScreen} />
      <Stack.Screen name="Splash2" component={SplashScreen2} />
      <Stack.Screen name="Tabs" component={MyTabs} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Signin3" component={Signin3} />
      <Stack.Screen name="Signin" component={Signin} />
      <Stack.Screen name="Signup" component={Signup} />
      <Stack.Screen name="Signup2" component={Signup2} />
      <Stack.Screen name="MenuDetail" component={MenuDetail} />
      <Stack.Screen name="Profile2" component={Profile2} />
      <Stack.Screen name="MerchantProfile" component={MerchantProfile} />
      <Stack.Screen name="Checkout" component={Checkout1} />
      <Stack.Screen name="Checkout2" component={Checkout2} />
      <Stack.Screen name="ESProfile" component={ESProfile} />
      <Stack.Screen name="Cart1b" component={Cart} />
    </Stack.Navigator>
  );
};

export default Appstack;
