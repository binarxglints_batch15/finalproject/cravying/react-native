import React, {Profiler} from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import Icon from 'react-native-vector-icons/FontAwesome5';
import Menu from '../screens/Menu';
import Order from '../screens/Order';
import Cart from '../screens/Cart';
import Profile from '../screens/Profile';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const Tab = createBottomTabNavigator();

const MyTabs = () => {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        headerShown: false,
        tabBarHideOnKeyboard: true,
        tabBarInactiveTintColor: 'grey',
        tabBarActiveTintColor: '#FF5353',
        tabBarStyle: {
          backgroundColor: 'white',
          paddingBottom: 5,
          paddingTop: 5,
          height: hp('6%'),
        },
        tabBarIcon: ({focused, size, color}) => {
          let iconName;
          if (route.name == 'Menu') {
            iconName = focused = 'hamburger';
          } else if (route.name == 'Cart') {
            iconName = focused = 'shopping-cart';
          } else if (route.name == 'Order') {
            iconName = focused = 'book';
          } else {
            iconName = focused = 'person-booth';
          }
          return <Icon name={iconName} size={15} color={color} />;
        },
      })}>
      <Tab.Screen name="Menu" component={Menu} />
      <Tab.Screen name="Cart" component={Cart} />
      <Tab.Screen name="Order" component={Order} />
      <Tab.Screen name="Profile" component={Profile} />
    </Tab.Navigator>
  );
};

export default MyTabs;
