import {useNavigation} from '@react-navigation/core';
import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default function Button(props) {
  const navigation = useNavigation();
  return (
    <View style={{alignItems: 'center'}}>
      <TouchableOpacity style={style.costumer} onPress={props.handleNavigation}>
        <Text style={style.costumerTxt}>{props.name}</Text>
      </TouchableOpacity>
    </View>
  );
}

const style = StyleSheet.create({
  costumerTxt: {
    color: '#FFFFFF',
    fontFamily: 'Poppins-Bold',
    fontSize: 16,
  },
  costumer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 2,
    borderColor: '#FF5353',
    backgroundColor: '#FF5353',
    borderRadius: 10,
    height: hp('8%'),
    width: wp('90%'),
    marginVertical: '3%',
  },
});
