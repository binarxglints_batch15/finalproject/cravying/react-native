import React from 'react';
import {View, Text} from 'react-native';
import {FloatingLabelInput} from 'react-native-floating-label-input';

export default function Input(props) {
  return (
    <View>
      <FloatingLabelInput
        value={props.value}
        staticLabel
        hintTextColor={'#80848D'}
        hint={props.hint}
        onChangeText={value => props.onTxt(value)}
        containerStyles={{
          borderWidth: 2,
          borderColor: '#E1E1E1',
          backgroundColor: '#FFFFFF',
          borderRadius: 5,
          marginHorizontal: '5%',
        }}
        inputStyles={{
          color: '#80848D',
          paddingHorizontal: 15,
          fontSize: 16,
        }}
      />
    </View>
  );
}
