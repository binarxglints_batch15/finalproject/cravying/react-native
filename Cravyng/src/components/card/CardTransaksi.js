import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const CardTransaksi = props => {
  return (
    <View style={style.container}>
      <View style={style.item}>
        <Text style={style.text}>{props.title}</Text>
        <Text style={style.numb}>{props.number}</Text>
      </View>
    </View>
  );
};

export default CardTransaksi;

const style = StyleSheet.create({
  container: {
    width: wp('70%'),
    height: hp('15%'),
    paddingVertical: 16,
    paddingHorizontal: 16,
    backgroundColor: '#FAF9FF',
    borderRadius: 10,
  },
  item: {
    flexDirection: 'column',
  },
  text: {
    fontSize: 14,
    fontFamily: 'ProximaNova-Reguler',
    color: '#868993',
  },
  numb: {
    color: '#616BC8',
    fontFamily: 'Poppins-Bold',
    fontSize: 21,
    top: 16,
  },
});
