import {useNavigation} from '@react-navigation/core';
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  Image,
  TouchableOpacity,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Icon2 from 'react-native-vector-icons/AntDesign';
import {Dimensions} from 'react-native';
export default function cardMenu(props) {
  const [take, setTake] = useState();
  const navigation = useNavigation();

  const windowWidth = Dimensions.get('window').width;
  const windowHeight = Dimensions.get('window').height;

  useEffect(() => {
    setTake(props.passing);
  }, [props.passing]);

  // console.log('INI DATA DI CARD', props.data.id);
  // console.log(windowHeight, windowWidth);
  return (
    <View>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('MenuDetail', {
            passingmenu: props.passing,
            passid: props.data.id,
          });
        }}>
        <ImageBackground
          imageStyle={{borderRadius: 7}}
          style={take == props.data.id ? styles.boximg1 : styles.boximg}
          source={props.data.imagemenu}>
          <View>
            {take == props.data.id ? (
              <View
                style={{
                  backgroundColor: 'rgba(255, 255, 255, 0.8)',
                  width: wp('43%'),
                  height: wp('43%'),
                }}>
                <View
                  style={{
                    backgroundColor: '#616BC8',
                    width: windowWidth * 0.1,
                    height: windowWidth * 0.1,
                    borderRadius: (windowWidth * 0.1) / 2,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginHorizontal: 10,
                    marginVertical: 10,
                  }}>
                  <Text
                    style={{
                      color: 'white',
                      fontSize: 14,
                      fontFamily: 'Proxima-Nova-Bold',
                    }}>
                    1
                  </Text>
                </View>
              </View>
            ) : (
              <View
                style={{
                  // backgroundColor: 'red',
                  width: windowWidth * 0.1,
                  height: windowWidth * 0.1,
                  borderRadius: (windowWidth * 0.1) / 2,
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginHorizontal: 10,
                  marginVertical: 10,
                }}>
                <Text
                  style={{
                    color: 'white',
                    fontSize: 14,
                    fontFamily: 'Proxima-Nova-Bold',
                  }}></Text>
              </View>
            )}
          </View>
          <View
            style={{
              width: wp('43%'),
              height: wp('43%'),
              justifyContent: 'flex-start',
              alignItems: 'flex-end',
              flexDirection: 'row',
            }}>
            <Icon
              style={styles.icon}
              name={props.data.icon}
              size={20}
              color="white"
            />
            <Text style={styles.text}>{props.data.menu}</Text>
          </View>
        </ImageBackground>
      </TouchableOpacity>
      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <View
          style={{
            justifyContent: 'center',
            width: wp('43%'),
            marginLeft: 20,
          }}>
          <Text style={styles.text1}>{props.data.menu1}</Text>
          <View style={styles.boxprice}>
            <Text style={styles.text2}>{props.data.price1}</Text>
            <Text style={styles.text3}>{props.data.pricediscount}</Text>
            <Icon2
              style={styles.icon2}
              name={props.data.icon2}
              size={15}
              color="red"
            />
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  boximg: {
    width: wp('43%'),
    height: wp('43%'),
    borderRadius: 20,
    resizeMode: 'cover',
    marginLeft: 20,
    marginTop: 13,
  },
  boximg1: {
    width: wp('43%'),
    height: wp('43%'),
    borderRadius: 20,
    resizeMode: 'cover',
    marginLeft: 20,
    marginTop: 15,
  },
  icon: {
    alignItems: 'baseline',
    marginHorizontal: 10,
  },

  text: {color: 'white', fontSize: 14, fontFamily: 'Proxima-Nova-Bold'},
  boxprice: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  text1: {
    color: 'black',
    marginTop: 4,
    fontSize: 14,
    fontWeight: '500',
  },
  text2: {
    color: 'black',
    marginVertical: 5,
    fontSize: 12,
    marginRight: 5,
  },
  text3: {
    color: '#868993',
    marginVertical: 5,
    fontSize: 12,
    marginRight: 5,
    textDecorationLine: 'line-through',
  },
  icon2: {
    marginHorizontal: 2,
  },
});
