import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {RadioButton} from 'react-native-paper';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const CardRadio = props => {
  const [checked, setChecked] = React.useState('first');
  return (
    <View>
      <View style={style.boxbot}>
        <Text style={style.text5}>{props.title1}</Text>
        <Text>*Required</Text>
      </View>
      <View>
        <Text>Select 1</Text>
      </View>
      <View style={style.boxradio}>
        <View style={style.radiobut}>
          <RadioButton
            value="first"
            color="#616BC8"
            status={checked === 'first' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('first')}
          />
          <Text>{props.menu1}</Text>
        </View>
        <View style={style.radiobut}>
          <RadioButton
            value="second"
            color="#616BC8"
            status={checked === 'second' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('second')}
          />
          <Text>{props.menu2}</Text>
        </View>
      </View>
    </View>
  );
};
export default CardRadio;

const style = StyleSheet.create({
  outer: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  container: {
    width: wp('90%'),
    alignItems: 'flex-start',
  },
  boxicon: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  boxbot: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 10,
    width: wp('90%'),
  },
  img: {
    width: wp('100%'),
    height: hp('30%'),
    resizeMode: 'cover',
  },
  text1: {
    color: 'black',
    marginVertical: 5,
    fontSize: 14,
  },
  text2: {
    color: 'black',
    marginVertical: 5,
    fontSize: 12,
    marginRight: 5,
  },
  text3: {
    color: '#868993',
    marginVertical: 5,
    fontSize: 12,
    marginRight: 5,
    textDecorationLine: 'line-through',
  },
  text4: {
    fontSize: 12,
    marginHorizontal: 8,
    color: '#FF5353',
  },
  text5: {
    fontSize: 16,
    fontFamily: 'Popins-Bold',
    color: 'black',
  },
  text6: {
    fontSize: 16,
    fontFamily: 'Popins-Bold',
    color: 'purple',
    fontWeight: '600',
  },
  boxprice: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textlong: {
    fontWeight: '400',
    fontSize: 14,
    lineHeight: 21,
    marginVertical: 17,
  },
  radiobut: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  boxradio: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    width: wp('100%'),
    marginVertical: 10,
  },
});
