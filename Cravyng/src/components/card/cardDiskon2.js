import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {styles} from 'react-native-floating-label-input/src/styles';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
export default function cardDiskon(props) {
  return (
    <View style={{flex: 1}}>
      <View style={style.container}>
        <View style={style.boxout}>
          <View style={style.box1}>
            <Text style={style.text}>Discount </Text>
            <Text style={style.text}>
              {props.disone}
              {props.dis1}
            </Text>
          </View>
          <View style={style.box2}>
            <Text></Text>
            <Text style={style.text1}>{props.dis} </Text>
            <Text style={style.text1}>off on minimum order value of</Text>
            <Text style={style.text1}>{props.rupiah}</Text>
          </View>
          <View>
            <Text style={style.text2}>Coupon ID: 828323</Text>
          </View>
        </View>
      </View>
    </View>
  );
}

const style = StyleSheet.create({
  container: {
    backgroundColor: '#616BC8',
    height: hp('10%'),
    borderRadius: 7,
    marginHorizontal: 5,
    paddingVertical: 20,
    justifyContent: 'center',
  },
  boxout: {
    marginLeft: 10,
    marginRight: 10,
  },
  box1: {
    flexDirection: 'row',
  },
  box2: {
    flexDirection: 'row',
  },
  text: {
    color: 'white',
    fontSize: 14,
    marginHorizontal: 3,
    fontFamily: 'Proxima-Nova-Bold',
  },
  text1: {
    color: 'white',
    fontSize: 12,
    marginHorizontal: 3,
    fontFamily: 'ProximaNova-Regular',
  },
  text2: {
    color: 'white',
    fontSize: 12,
    fontWeight: 'bold',
  },
});
