import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const CardCheckout = props => {
  return (
    <View style={styles.container}>
      <View style={styles.box}>
        <View style={styles.rowtext}>
          <Text style={styles.text}>Order Id</Text>
          <Text style={styles.text5}>Total Order</Text>
        </View>
        <View style={styles.rowtext}>
          <Text style={styles.text1}>{props.data.DateId}</Text>
          <Text style={styles.text2}>{props.data.PriceOrder}</Text>
        </View>
        <View style={styles.rowtext}>
          <Text style={styles.text}>Date</Text>
          <Text style={styles.text3}>Status</Text>
        </View>
        <View style={styles.rowtext}>
          <Text style={styles.text1}>{props.data.Date}</Text>
          <Text
            style={{
              fontSize: 14,
              fontFamily: 'ProximaNova-Regular',
              marginTop: 5,
              marginRight: 10,
              width: wp('14%'),
              color: props.data.Status == 'Paid' ? 'green' : 'red',
            }}>
            {props.data.Status}
          </Text>
        </View>
      </View>
    </View>
  );
};

export default CardCheckout;
const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 20,
    marginTop: 15,
  },
  box: {
    width: wp('90%'),
    height: hp('16%'),
    borderRadius: 7,
    borderColor: '#D3D9FF',
    borderWidth: 2,
    paddingVertical: 5,
  },
  rowtext: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: 90,
    marginLeft: 30,
  },
  text: {
    fontSize: 12,
    fontFamily: 'ProximaNova-Regular',
    marginTop: 10,
  },
  text2: {
    fontSize: 14,
    fontFamily: 'ProximaNova-Regular',
    color: 'black',
    marginTop: 5,
  },
  text1: {
    fontSize: 14,
    fontFamily: 'ProximaNova-Regular',
    color: 'black',
    marginTop: 5,
  },
  text3: {
    marginTop: 10,
    marginRight: 35,
    fontSize: 12,
    fontFamily: 'ProximaNova-Regular',
    color: 'black',
  },
  text4: {
    fontSize: 14,
    fontFamily: 'ProximaNova-Regular',
    marginTop: 5,
    marginRight: 10,
    width: wp('14%'),
  },
  text5: {
    fontSize: 12,
    fontFamily: 'ProximaNova-Regular',
    marginTop: 10,
    marginRight: 10,
  },
});
