import React, {useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const select = true;

const CardCart = props => {
  const [plus, setPlus] = useState(1);
  // console.log('INI PLUS', plus);

  return (
    <View style={style.container}>
      <View style={style.top}>
        <View style={{flexDirection: 'column'}}>
          <Text style={style.txttop}>{props.data.title}</Text>
          <View style={{flexDirection: 'row'}}>
            <Text style={style.txtmid}>{props.data.price}</Text>
            <Text style={style.disc}>{props.data.discount}</Text>
          </View>
          {Object.values(props.data.orders).map((key, index) => (
            <Text style={style.textapt}>{key}</Text>
          ))}
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              paddingVertical: 5,
            }}>
            <Icons name="clipboard-text" size={15} color={'#616BC8'} />
            <Text style={style.note}>{props.data.note}</Text>
          </View>
        </View>
        <View style={{flexDirection: 'row', paddingVertical: 5}}>
          {plus == 1 ? (
            <TouchableOpacity
              onPress={() => {
                props.press(props.data.title);
              }}>
              <Icon name="trash" size={15} color={'#FF5353'} />
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              onPress={() => {
                if (plus > 1) {
                  setPlus(plus - 1);
                }
              }}>
              <Icon name="minus" size={15} color={'#FF5353'} />
            </TouchableOpacity>
          )}
          <Text style={{marginHorizontal: 15, color: '#000000'}}>{plus}</Text>
          <TouchableOpacity onPress={() => setPlus(plus + 1)}>
            <Icon name="plus" size={15} color={'#FF5353'} />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default CardCart;

const style = StyleSheet.create({
  container: {
    flex: 1,
  },
  top: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomColor: '#D3D9FF',
    borderBottomWidth: 1,
    width: '100%',
    height: hp('20%'),
    paddingVertical: 15,
  },
  txttop: {
    fontFamily: 'Proxima-NovaRgBold',
    fontSize: 14,
    color: '#000000',
    paddingVertical: 5,
  },
  textapt: {
    fontFamily: 'Proxima-NovaSRG',
    fontSize: 14,
    color: '#868993',
    paddingVertical: 5,
  },
  disc: {
    fontFamily: 'ProximaNova-Reguler',
    fontSize: 14,
    color: '#868993',
    paddingHorizontal: 10,
    textDecorationLine: 'line-through',
  },
  note: {
    fontSize: 14,
    marginHorizontal: 5,
    color: '#616BC8',
    fontFamily: 'Proxima-Nova-Bold',
  },
  txtmid: {
    fontSize: 14,
    fontFamily: 'ProximaNova-Reguler',
    color: '#313440',
  },
});
