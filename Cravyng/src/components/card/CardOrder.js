import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const CardOrder = props => {
  return (
    <View style={style.container}>
      <View style={style.item}>
        <View style={{height: 120}}>
          <Text style={style.text}>{props.title}</Text>
          <Text style={style.numb}>{props.id}</Text>
          <Text style={style.text}>{props.tgl}</Text>
          <Text style={style.numb}>{props.date}</Text>
        </View>
        <View style={{height: 120, paddingHorizontal: 70}}>
          <Text style={style.text}>{props.total}</Text>
          <Text style={style.numb}>{props.numb}</Text>
          <Text style={style.text}>{props.status}</Text>
          <Text style={style.pay}>{props.pay}</Text>
        </View>
      </View>
    </View>
  );
};

export default CardOrder;

const style = StyleSheet.create({
  container: {
    width: wp('90%'),
    height: hp('20%'),
    paddingVertical: 16,
    paddingHorizontal: 16,
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    borderColor: '#D3D9FF',
    borderWidth: 1,
  },
  item: {
    flexDirection: 'row',
  },
  text: {
    fontSize: 12,
    fontFamily: 'ProximaNova-Reguler',
    color: '#868993',
    marginVertical: 5,
  },
  numb: {
    color: '#313440',
    fontFamily: 'ProximaNova-Reguler',
    fontSize: 14,
    marginVertical: 2,
    marginBottom: 15,
  },
  pay: {
    color: '#328E8E',
    fontFamily: 'ProximaNova-Reguler',
    fontSize: 14,
    marginVertical: 2,
  },
});
