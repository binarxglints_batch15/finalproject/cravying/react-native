import React, {useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {heightPercentageToDP} from 'react-native-responsive-screen';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const CardCheckout1 = props => {
  // console.log('INI DARI ', props);
  return (
    <View>
      <View style={styles.container}>
        <View>
          <Text style={styles.textfront}>1X</Text>
        </View>
        <View style={styles.boxtextright}>
          <Text style={styles.text}>{props.data.namemenu}</Text>
          <View style={styles.textinrow}>
            <Text style={styles.text}>{props.data.price}</Text>
            <Text style={styles.text2}>{props.data.pricedis}</Text>
          </View>
          {Object.values(props.data.toping).map((key, index) => (
            <Text style={styles.textgrey}>{key}</Text>
          ))}

          <Text style={styles.text3}>Notes {props.data.notes}</Text>
        </View>
      </View>
      <View style={styles.line}></View>
    </View>
  );
};

export default CardCheckout1;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: 'white',
    width: wp('90%'),
    marginHorizontal: 20,
    alignItems: 'flex-start',
  },
  textfront: {
    marginRight: 3,
    fontFamily: 'Poppins-Bold',
    color: 'black',
    fontSize: 11,
  },
  boxtextright: {
    marginHorizontal: 26,
  },
  text: {
    fontFamily: 'Proxima-NovaRgBold',
    color: 'black',
    marginBottom: 4,
    fontSize: hp('1.8%'),
  },
  textgrey: {
    fontFamily: 'ProximaNova-Regular',
    color: 'grey',
    marginVertical: 5,
    fontSize: hp('1.6%'),
  },
  text2: {
    textDecorationLine: 'line-through',
    marginHorizontal: 10,
    marginBottom: 4,
    fontSize: hp('1.8%'),
  },
  textinrow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  text3: {
    marginVertical: 10,
    fontSize: hp('1.6%'),
  },
  line: {
    height: 1,
    width: wp('90%'),
    backgroundColor: '#D3D9FF',
    marginHorizontal: 20,
    marginVertical: 15,
  },
});
