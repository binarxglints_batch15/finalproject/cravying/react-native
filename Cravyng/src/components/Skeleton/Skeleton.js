import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
export default function SkeletonEvent() {
  return (
    <View
      style={{
        backgroundColor: '#FFFFFF',
        marginHorizontal: 17,
        marginVertical: 17,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        height: hp('28%'),
        width: wp('43%'),
      }}>
      <SkeletonPlaceholder style={style.skeleton}>
        <View
          style={{
            justifyContent: 'flex-end',
            borderRadius: 10,
          }}>
          <View
            style={{
              height: hp('20%'),
              width: wp('43%'),
              borderRadius: 10,
            }}></View>
          <View>
            <View
              style={{
                height: hp('2%'),
                width: wp('33%'),
                marginVertical: 10,
              }}></View>
            <View
              style={{
                height: hp('2%'),
                width: wp('38%'),
              }}></View>
          </View>
        </View>
      </SkeletonPlaceholder>
    </View>
  );
}

const style = StyleSheet.create({
  skeleton: {
    backgroundColor: '#b7bbbe',
    speed: 300,
    direction: 'rtl',
  },
});
