import React, {useState} from 'react';
import {View, Text} from 'react-native';
import {FloatingLabelInput} from 'react-native-floating-label-input';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

export default function Input(props) {
  const [Show, setShow] = useState(false);

  return (
    <View>
      <FloatingLabelInput
        isPassword
        togglePassword={Show}
        value={props.value}
        staticLabel
        hintTextColor={'#80848D'}
        hint={props.hint}
        onChangeText={value => props.onTxt(value)}
        customShowPasswordComponent={
          <FontAwesome name="eye" size={20} color={'#999999'} />
        }
        customHidePasswordComponent={
          <FontAwesome name="eye-slash" size={20} color={'#999999'} />
        }
        containerStyles={{
          borderWidth: 2,
          borderColor: '#E1E1E1',
          backgroundColor: '#FFFFFF',
          borderRadius: 5,
          marginHorizontal: '5%',
          paddingRight: 15,
        }}
        inputStyles={{
          color: '#80848D',
          paddingHorizontal: 15,
          fontSize: 16,
        }}
      />
    </View>
  );
}
