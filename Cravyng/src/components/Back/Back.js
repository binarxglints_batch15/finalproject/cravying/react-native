import {useNavigation} from '@react-navigation/core';
import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';

export default function Back(props) {
  const navigation = useNavigation();
  return (
    <View>
      <View>
        <TouchableOpacity
          style={{flexDirection: 'row'}}
          onPress={() => navigation.navigate(props.navigate)}>
          <Image
            source={require('../../assets/image/back.png')}
            style={style.img}
          />
          <Text style={style.headerTxt}>{props.name}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
const style = StyleSheet.create({
  img: {
    marginTop: 27,
    marginLeft: 24,
  },
  headerTxt: {
    marginLeft: 13,
    marginTop: 22,
    fontSize: 16,
    color: 'black',
  },
});
