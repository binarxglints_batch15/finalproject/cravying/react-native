import React, {useState} from 'react';
import {
  View,
  Text,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import Modal from 'react-native-modal';

const HeaderProfile = props => {
  const [isModalVisible, setModalVisible] = useState(false);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  return (
    <View style={styles.container}>
      <StatusBar translucent={true} backgroundColor={'transparent'} />
      <View style={styles.head}>
        <TouchableOpacity onPress={() => alert('TES')}>
          <Icon name={props.iconright} size={20} color="#313440" />
        </TouchableOpacity>
        <View style={styles.text}>
          <Text style={styles.headtxt}>{props.title}</Text>
        </View>
        <TouchableOpacity onPress={toggleModal}>
          <Icon2 name={props.iconleft} size={25} color="#313440" />
        </TouchableOpacity>
      </View>
      <Modal
        hasBackdrop={true}
        isVisible={isModalVisible}
        testID={'modal'}
        style={styles.view}>
        <View
          style={{
            width: wp('100%'),
            height: hp('32%'),
            backgroundColor: '#FFFFFF',
            justifyContent: 'flex-end',
            borderRadius: 16,
            borderBottomEndRadius: 0,
            borderBottomLeftRadius: 0,
            padding: 20,
          }}>
          <View
            style={{
              alignItems: 'flex-start',
            }}>
            <Text
              style={{
                fontSize: 14,
                fontFamily: 'Poppins-Reguler',
                color: '#000000',
                paddingLeft: 10,
                paddingBottom: 40,
              }}>
              Are you sure to logout?
            </Text>
          </View>
          <View>
            <TouchableOpacity onPress={toggleModal} style={styles.touch}>
              <Text style={styles.button}>Ups, no</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touch1}>
              <Text style={styles.button1}>Yes</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default HeaderProfile;

const styles = StyleSheet.create({
  container: {
    width: wp('100%'),
    height: hp('12%'),
    borderBottomWidth: 1,
    backgroundColor: '#FFFFFF',
    borderBottomColor: '#D3D9FF',
    justifyContent: 'center',
    alignItems: 'center',
  },
  head: {
    width: wp('85%'),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 10,
    marginTop: 30,
    marginLeft: 30,
  },
  headtxt: {
    fontFamily: 'Poppins-Reguler',
    fontSize: 16,
    color: '#313440',
  },
  text: {
    alignItems: 'center',
  },
  view: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  button: {
    fontSize: 16,
    fontFamily: 'Poppins-Reguler',
    alignSelf: 'center',
    color: '#FFFFFF',
    marginVertical: 10,
  },
  button1: {
    fontSize: 16,
    fontFamily: 'Poppins-Reguler',
    alignSelf: 'center',
    color: '#FF5353',
    marginVertical: 10,
  },
  touch: {
    borderColor: '#FF5353',
    borderWidth: 1,
    borderRadius: 10,
    backgroundColor: '#FF5353',
    height: hp('6%'),
    marginVertical: 20,
  },
  touch1: {
    borderColor: '#FF5353',
    borderWidth: 1,
    borderRadius: 10,
    backgroundColor: '#FFFFFF',
    height: hp('6%'),
  },
});
