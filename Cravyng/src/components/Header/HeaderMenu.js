import React from 'react';
import {View, Text, StyleSheet, ImageBackground, Image} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Star from 'react-native-vector-icons/MaterialCommunityIcons';

export default function HeaderMenu(props) {
  return (
    <View style={style.container}>
      <ImageBackground
        style={style.bgimage}
        source={require('../../assets/image/pictureheader.png')}>
        <View style={style.box}>
          <Text style={style.textHead}>Heavenly Taste</Text>
          <View style={style.textbot}>
            <Star
              name="star-circle"
              size={16}
              color={'white'}
              style={style.star}
            />
            <Text style={style.text2}>{props.text1}</Text>
          </View>
        </View>
      </ImageBackground>
    </View>
  );
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'red',
    position: 'absolute',
  },
  bgimage: {
    height: hp('22%'),
    width: wp('100%'),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
  },
  textHead: {
    fontSize: 24,
    fontFamily: 'Poppins-Bold',
    color: 'white',
  },
  box: {
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
    flex: 1,
    height: hp('22%'),
    width: wp('100%'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  textbot: {
    flexDirection: 'row',
    marginVertical: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text2: {
    fontSize: 16,
    color: 'white',
    fontFamily: 'Poppins-Light',
  },
});
