import React, {useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Dropdown} from 'react-native-element-dropdown';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const data = [
  {label: 'Today', value: 'Today'},
  {label: 'Period', value: 'Period'},
  {label: '28/03/21', value: '28/03/21'},
  {label: '01/04/21', value: '01/04/21'},
];

const DropdownMerch = props => {
  const [value, setValue] = useState(null);
  const [isFocus, setIsFocus] = useState(false);

  const renderlabel = () => {
    if (value || isFocus) {
      return (
        <Text style={[styles.label, isFocus && {color: '#3E3E3E'}]}>
          {props.label == 'Date'
            ? 'Date'
            : props.label == 'From'
            ? 'From'
            : 'To'}
        </Text>
      );
    }
    return null;
  };

  return (
    <View style={styles.container}>
      {renderlabel()}
      <Dropdown
        style={[styles.dropdown, isFocus && {borderColor: '#E5E5E5;'}]}
        placeholderStyle={styles.placeholderStyle}
        selectedTextStyle={styles.selectedTextStyle}
        inputSearchStyle={styles.inputSearchStyle}
        data={data}
        search
        maxHeight={200}
        labelField="label"
        valueField="value"
        placeholder={!isFocus ? 'Select Category' : '...'}
        searchPlaceholder="Search..."
        value={value}
        onFocus={() => setIsFocus(true)}
        onBlur={() => setIsFocus(false)}
        onChange={item => {
          // console.log(item.value);
          setValue(item.value);
          setIsFocus(false);
          props.label == 'Date' && props.select(item.value);
        }}
      />
    </View>
  );
};

export default DropdownMerch;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    padding: 16,
    paddingVertical: 16,
  },
  dropdown: {
    width: wp('91%'),
    height: hp('7%'),
    borderColor: '#E5E5E5',
    borderWidth: 0.5,
    borderRadius: 5,
    paddingHorizontal: 8,
  },
  icon: {
    marginRight: 5,
  },
  label: {
    position: 'absolute',
    backgroundColor: 'white',
    left: 30,
    top: 8,
    zIndex: 999,
    paddingHorizontal: 8,
    fontSize: 14,
    color: 'black',
  },
  placeholderStyle: {
    fontSize: 16,
    color: 'black',
  },
  selectedTextStyle: {
    fontSize: 16,
    paddingLeft: 14,
    color: 'black',
  },
  inputSearchStyle: {
    height: 40,
    fontSize: 16,
    color: 'black',
  },
});
