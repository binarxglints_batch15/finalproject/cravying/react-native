import React from 'react';
import {View, Image, TouchableOpacity, StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default function Icon() {
  return (
    <View style={[style.image]}>
      <TouchableOpacity>
        <Image
          source={require('../../assets/image/Google.png')}
          style={style.google}
        />
      </TouchableOpacity>
      <TouchableOpacity>
        <Image
          source={require('../../assets/image/Facebook.png')}
          style={style.FB}
        />
      </TouchableOpacity>
    </View>
  );
}
const style = StyleSheet.create({
  FB: {
    height: hp('6.1%'),
    width: wp('13%'),
  },
  google: {
    height: hp('6.1%'),
    width: wp('13%'),
    marginRight: '5%',
  },
  image: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: '3%',
  },
});
