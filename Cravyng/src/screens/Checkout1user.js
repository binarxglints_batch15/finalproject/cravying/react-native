import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import HeaderProfile from '../components/Header/HeaderProfile';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import CardCheckout1 from '../components/card/CardCheckout1';
import {FloatingLabelInput} from 'react-native-floating-label-input';
import {RadioButton} from 'react-native-paper';
const Checkout1 = props => {
  const [Price, setPrice] = useState();
  const [Discount, setDiscount] = useState();
  const [Special, setSpecial] = useState();

  const [birthday, setBirthday] = useState('');
  const [name, setName] = useState();
  const [checked, setChecked] = React.useState('first');
  const checkout = [
    {
      id: '1',
      namemenu: 'Menu A for 1 Pax',
      price: 'Rp 176.000',
      pricedis: 'Rp 220.000',
      toping: {
        toping1: '+ Scrambled Egg with Tomato',
        toping2: '+ Chicken in Sichuan Chili Oil Sauce',
      },
      notes: 'no mayo',
    },
    {
      id: '2',
      namemenu: 'Chongqing Spicy Chick...',
      price: 'Rp 176.000',
      pricedis: 'Rp 220.000',
      toping: {},
      notes: 'no salad',
    },
  ];

  const renderItem = ({item}) => {
    // console.log('INI DARI RENDER ITEM', item);
    return <CardCheckout1 data={item} />;
  };

  return (
    <ScrollView style={{backgroundColor: 'white'}}>
      <View>
        <HeaderProfile iconleft="keyboard-arrow-left" title="Checkout" />
        <View style={styles.container}>
          <View style={styles.textrow}>
            <Icon
              style={styles.iconstyle}
              name="storefront"
              size={15}
              color="#C2C4CD"
            />
            <Text style={styles.textup}>Heavenly Taste</Text>
          </View>

          <View style={styles.textrow2}>
            <Icon
              style={styles.iconstyle}
              name="person"
              size={15}
              color="#C2C4CD"
            />
            <Text style={styles.textup}>Rara</Text>
          </View>
        </View>
        <View>
          <FlatList
            data={checkout}
            renderItem={renderItem}
            keyExtractor={(index, i) => i}
          />
        </View>
        <View style={styles.boxmain}>
          <View style={styles.box}>
            <View style={styles.text}>
              <Text style={styles.textmain}>Price</Text>
              <Text style={styles.textmain}>Rp 440.000</Text>
            </View>
            <View style={styles.text}>
              <Text style={styles.textmain}>Discount</Text>
              <Text style={styles.textmain}>-Rp 88.000</Text>
            </View>
            <View style={styles.text}>
              <Text style={styles.textmain}>Special Offers</Text>
              <Text style={styles.textmain}>-Rp 20.000</Text>
            </View>
            <View style={styles.line}></View>
            <View style={styles.text}>
              <Text style={styles.textmain2}>Total Payment</Text>
              <Text style={styles.textmain2}>Rp 352.000</Text>
            </View>

            <View style={styles.send}>
              <Text style={styles.textmain2}>Send Receipt</Text>
              <Text style={styles.textmain3}>
                Please input email address to send the receipt
              </Text>
              <View style={{marginBottom: 30}}>
                <FloatingLabelInput
                  label={'Email'}
                  value={name}
                  // hintTextColor={'grey'}
                  // hint="Fullname"
                  onChangeText={value => setFullname(value)}
                  containerStyles={{
                    borderWidth: 2,
                    paddingHorizontal: 10,
                    borderColor: 'grey',
                    borderRadius: 5,
                  }}
                  customLabelStyles={{
                    colorFocused: 'grey',
                    fontSizeFocused: 10,
                  }}
                  labelStyles={{
                    paddingHorizontal: 5,
                    borderRadius: 5,
                  }}
                  inputStyles={{
                    color: 'black',
                    paddingHorizontal: 5,
                  }}
                />
              </View>
            </View>
            <View>
              <Text style={styles.textmain2}>Payment Method</Text>
              <Text style={styles.textmain3}>Select payment method</Text>
              <View style={styles.radiobut}>
                <RadioButton
                  value="first"
                  color="#616BC8"
                  status={checked === 'first' ? 'checked' : 'unchecked'}
                  onPress={() => setChecked('first')}
                />
                <Text>Bank</Text>
              </View>
              <View style={styles.radiobut}>
                <RadioButton
                  value="second"
                  color="#616BC8"
                  status={checked === 'second' ? 'checked' : 'unchecked'}
                  onPress={() => setChecked('second')}
                />
                <Text>Transfer</Text>
              </View>
              <View style={styles.radiobut}>
                <RadioButton
                  value="third"
                  color="#616BC8"
                  status={checked === 'third' ? 'checked' : 'unchecked'}
                  onPress={() => setChecked('third')}
                />
                <Text>Virtual Account</Text>
              </View>
              <View style={styles.radiobut}>
                <RadioButton
                  value="four"
                  color="#616BC8"
                  status={checked === 'four' ? 'checked' : 'unchecked'}
                  onPress={() => setChecked('four')}
                />
                <Text>Credit/Debit Card</Text>
              </View>
            </View>
          </View>
          <TouchableOpacity style={styles.footer}>
            <View style={styles.button}>
              <Text style={styles.textbutton}>Pay</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

export default Checkout1;

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 20,
    backgroundColor: 'white',
  },
  iconstyle: {
    marginVertical: 5,
    marginRight: 10,
  },
  boxmain: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  line: {
    height: 1,
    width: wp('90%'),
    backgroundColor: '#D3D9FF',

    marginVertical: 1,
  },
  box: {
    width: '90%',
  },
  text: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 8,
  },
  textrow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
  },
  textrow2: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 35,
  },
  textup: {
    fontSize: 14,
    fontFamily: 'ProximaNova-Regular',
  },
  textmain: {
    fontSize: 12,
    fontFamily: 'Proxima-Nova-Regular',
    color: '#313440',
  },
  textmain2: {
    fontSize: 14,
    fontFamily: 'Proxima-Nova-Bold',
    color: '#313440',
  },
  send: {
    marginTop: 30,
  },
  textmain3: {
    fontFamily: 'ProximaNova-Regular',
    color: 'grey',
    marginVertical: 10,
    fontSize: 14,
  },
  container2: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  radiobut: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  footer: {
    height: hp('11%'),
    width: wp('100%'),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderTopColor: 'red',
    borderRadius: 10,
    marginVertical: 10,
  },
  button: {
    width: wp('85%'),
    height: hp('8%'),
    backgroundColor: '#FF5353',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  textbutton: {
    color: 'white',
    fontSize: 16,
    fontFamily: 'Poppins-Bold',
  },
});
