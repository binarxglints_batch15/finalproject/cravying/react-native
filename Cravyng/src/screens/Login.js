import React, {useState} from 'react';
import {View, Text, Image, StyleSheet, LogBox} from 'react-native';
import Input from '../components/Input/Input';
import Back from '../components/Back/Back';
import Button from '../components/Button/Button';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useNavigation} from '@react-navigation/core';
import {connect} from 'react-redux';
import signup from '../redux/redux/signup';
LogBox.ignoreLogs(['Reanimated 2']);

const Login = props => {
  const [name, setName] = useState('');
  const navigation = useNavigation();
  const [email, setGabungan] = useState(props.route.params.email);
  const [password, setgabungan2] = useState(props.route.params.password);
  const [confirmPassword, setConfrimPassword] = useState(
    props.route.params.confirmPassword,
  );
  const dataSignupuser = {
    name,
    email,
    password,
    confirmPassword,
  };

  console.log(dataSignupuser);
  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <View style={{marginVertical: 15, backgroundColor: 'white'}}>
        <Back name="Back" />
      </View>
      <View style={style.chiken}>
        <Image source={require('../assets/image/chiken.png')} />
      </View>
      <View style={style.hello}>
        <Text style={style.helloTxt}>Hello!</Text>
      </View>
      <View style={style.continue}>
        <Text style={style.continueTxt}>
          To continue, please enter your name
        </Text>
      </View>
      <View style={{marginVertical: '5%'}}>
        <Input hint="Your Name" value={name} onTxt={setName} />
      </View>
      <View>
        <Button
          name="Continue"
          handleNavigation={() =>
            props.signup(dataSignupuser, props.navigation)
          }
        />
      </View>
    </View>
  );
};
const reduxDispatchSignup = dispacth => ({
  signup: (dataSignupuser, navi) =>
    dispacth({type: 'SIGNUP_START', data: dataSignupuser, navigasi: navi}),
});

export default connect(null, reduxDispatchSignup)(Login);

const style = StyleSheet.create({
  continueTxt: {
    color: '#868993',
    fontSize: 16,
  },
  continue: {
    marginLeft: '4%',
    marginTop: '1%',
  },
  hello: {
    marginLeft: '4%',
    marginTop: '5%',
  },
  helloTxt: {
    fontFamily: 'Poppins-Bold',
    fontSize: 31,
    color: '#313440',
  },
  chiken: {
    marginLeft: '4%',
    marginTop: '8%',
  },
});
