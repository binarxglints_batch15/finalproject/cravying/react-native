import React, {useState, useEffect} from 'react';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  LogBox,
  StatusBar,
  ScrollView,
  BackHandler,
} from 'react-native';
import Icon from '../components/Icon/Icon';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Input from '../components/Input/Input';
import InPassword from '../components/InPassword/InPAssword';
import Button from '../components/Button/Button';
import {FloatingLabelInput} from 'react-native-floating-label-input';
const Signin = props => {
  const [Email, setEmail] = useState('');
  const [Password, setPassword] = useState('');

  const goBack = () => {
    props.navigation.navigate('Splash');
    return true;
  };
  // console.log(Password);
  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', goBack);
    return () => BackHandler.removeEventListener('hardwareBackPress', goBack);
  }, []);

  return (
    <ScrollView style={{backgroundColor: 'white', flex: 1}}>
      <StatusBar translucent={true} backgroundColor={'transparent'} />
      <View style={{marginVertical: 20}}>
        <TouchableOpacity style={{flexDirection: 'row'}}>
          <Image
            source={require('../assets/image/back.png')}
            style={style.img}
          />
          <Text style={style.headerTxt}>Back</Text>
        </TouchableOpacity>
      </View>
      {/* <View style={style.tacos}>
        <Image source={require('../assets/image/tacos.png')} />
      </View> */}
      <View style={{marginTop: '22%'}}>
        <Text style={style.signin}>Sign in</Text>
      </View>
      <View style={{marginVertical: '4%'}}>
        <Input hint="Email" value={Email} onTxt={setEmail} />
      </View>
      <View>
        <InPassword hint="Password" value={Password} onTxt={setPassword} />
      </View>
      <View style={{marginTop: '4%'}}>
        <Button name="Sign in" navigate="Tabs" />
      </View>
      <View style={style.or}>
        <Text style={style.txt}>____________________ </Text>
        <Text style={style.txt}>or</Text>
        <Text style={style.txt}> ____________________</Text>
      </View>
      <View style={{marginBottom: '15%'}}>
        <Icon />
      </View>
      <View style={style.signup}>
        <Text>Don't have an account? </Text>
        <TouchableOpacity>
          <Text style={{color: 'blue'}}> Sign up</Text>
        </TouchableOpacity>
      </View>
      {/* <View style={{ flexDirection: 'row', justifyContent: 'center', marginBottom: '10%' }}>
        <TouchableOpacity>
          <Text style={{ color: 'blue' }} >Sign up as Costumer</Text>
        </TouchableOpacity>
      </View> */}
    </ScrollView>
  );
};

export default Signin;

const style = StyleSheet.create({
  signup: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: '5%',
    marginBottom: '15%',
  },
  or: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  txt: {
    fontSize: 16,
    color: '#868993',
  },
  signin: {
    marginHorizontal: '4%',
    fontFamily: 'Poppins-Bold',
    fontSize: 31,
    color: 'black',
  },
  tacos: {
    marginTop: '15%',
    marginLeft: '4%',
  },
  img: {
    marginTop: 27,
    marginLeft: 24,
  },
  headerTxt: {
    marginLeft: 13,
    marginTop: 22,
    fontSize: 16,
    color: '#C2C4CD',
  },
});
