import {useNavigation} from '@react-navigation/core';
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Pressable,
} from 'react-native';
import HeaderProfile from '../../components/Header/HeaderProfile';
import Menu from '../Menu';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {RadioButton} from 'react-native-paper';
import Icon from 'react-native-vector-icons/AntDesign';
import CardRadio from '../../components/card/CardRadio';
import FoterDetail from '../../components/Footer/FoterDetail';
import Modal from 'react-native-modal';
const MenuDetail = props => {
  const navigation = useNavigation();
  const [isModalVisible2, setModalVisible2] = useState();
  const [Loading, setLoading] = useState();
  const [count, setCount] = useState(1);

  // console.log('INI DATA DI DETAIL DARI CARD ', props.route.params?.passid);
  const toggleModal2 = props => {
    setModalVisible2(!isModalVisible2);
  };

  useEffect(() => {
    setModalVisible2(true);
    setTimeout(() => {
      setModalVisible2(false);
    }, 2000);
  }, []);
  return (
    <View style={styles.outer}>
      <HeaderProfile
        title="Details"
        handleNavigationR={() => navigation.navigate('Menu')}
        iconright="cross"
      />
      {/* MODAL LOADING*/}
      <Modal
        // hasBackdrop={true}
        hasBackdrop={true}
        isVisible={isModalVisible2}
        onBackdropPress={toggleModal2}
        testID={'modal'}
        style={{justifyContent: 'center', alignItems: 'center', maargin: 0}}>
        <View>
          <Image
            style={{borderRadius: 10, width: wp('23'), height: wp('23')}}
            source={require('../../assets/image/logo.png')}></Image>
        </View>
      </Modal>

      <ScrollView
        style={{
          backgroundColor: 'red',
        }}>
        <View style={styles.outer2}>
          <Image
            style={styles.img}
            source={require('../../assets/image/gambar2.png')}></Image>
          <View style={styles.container}>
            <Text style={styles.text1}>Menu A for 1 Pax</Text>
            <View style={styles.boxprice}>
              <Text style={styles.text2}>Rp 176.000</Text>
              <Text style={styles.text3}>Rp 220.000</Text>
              <Icon name="tago" size={15} color="red" />
            </View>
            <View style={styles.boxicon}>
              <Icon name="like1" size={20} color="#FF5353" />
              <Text style={styles.text4}>Recommended</Text>
            </View>
            <View>
              <Text style={styles.textlong}>
                Scramble egg with tomato / Stir-fried String Beans ( Choose 1 )
                + Chicken In Sichuan Chili Oil Sauce / Sliced Beef and Ox tongue
                in Chill Sauce ( Choose 1 ) + Rice + Homemade Honey Black Tea
              </Text>
            </View>
            <CardRadio
              title1="Chef of Veg"
              menu1="Scrambled Egg with Tomato"
              menu2="Stir-fried String Beans"
            />
            <CardRadio
              title1="Chef of Veg"
              menu1="Scrambled Egg with Tomato"
              menu2="Stir-fried String Beans"
            />
            <View style={styles.box2}>
              <Text style={styles.text5}>Special Instructions</Text>
              <Text style={styles.text7}>
                Any specific preferences? Let us know.
              </Text>
            </View>
            <View style={styles.texbox}>
              <TextInput
                placeholder="e.g. no mayo"
                multiline={true}></TextInput>
            </View>
          </View>

          <View style={styles.boxfoter}>
            <View style={styles.line} />
            <View style={styles.infooter}>
              <TouchableOpacity onPress={() => setCount(count - 1)}>
                <Icon name="plus" size={20} color="black" />
              </TouchableOpacity>
              <TextInput style={styles.textcart}>
                <Text>{count}</Text>
              </TextInput>
              <TouchableOpacity onPress={() => setCount(count + 1)}>
                <Icon name="plus" size={20} color="#FF5353" />
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.buttoncart}
                onPress={() =>
                  navigation.navigate('Menu', {
                    action: props.route.params.passid,
                  })
                }>
                <Text style={styles.textbuttonchart}>Add to Cart</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default MenuDetail;

const styles = StyleSheet.create({
  outer: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  outer2: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  container: {
    width: wp('90%'),
    alignItems: 'flex-start',
  },
  boxicon: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  boxbot: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 10,
    width: wp('90%'),
  },
  img: {
    width: wp('100%'),
    height: hp('30%'),
    resizeMode: 'cover',
  },
  text1: {
    color: 'black',
    marginVertical: 5,
    fontSize: 14,
  },
  text2: {
    color: 'black',
    marginVertical: 5,
    fontSize: 12,
    marginRight: 5,
  },
  text3: {
    color: '#868993',
    marginVertical: 5,
    fontSize: 12,
    marginRight: 5,
    textDecorationLine: 'line-through',
  },
  text4: {
    fontSize: 12,
    marginHorizontal: 8,
    color: '#FF5353',
  },
  text5: {
    fontSize: 16,
    fontFamily: 'Popins-Bold',
    color: 'black',
  },
  text6: {
    fontSize: 16,
    fontFamily: 'Popins-Bold',
    color: 'purple',
    fontWeight: '600',
  },
  text7: {
    fontSize: 14,
    fontWeight: '400',
  },
  boxprice: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textlong: {
    fontWeight: '400',
    fontSize: 14,
    lineHeight: 21,
    marginVertical: 17,
  },
  radiobut: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  boxradio: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    width: wp('100%'),
    marginVertical: 10,
  },
  box2: {
    marginVertical: 20,
  },
  texbox: {
    borderRadius: 7,
    borderWidth: 1,
    borderColor: 'grey',
    width: wp('90%'),
    height: hp('27%'),
  },
  boxfoter: {
    height: hp('12%'),
    width: wp('100%'),
    elevation: 2,
    backgroundColor: 'white',
    justifyContent: 'center',
    marginTop: 20,
    marginBottom: 40,
  },
  infooter: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    height: hp('12%'),
  },
  textcart: {
    fontSize: 16,
    fontFamily: 'Poppins-Black',
    color: 'black',
  },
  buttoncart: {
    borderRadius: 10,
    backgroundColor: '#FF5353',
    height: hp('8%'),
    width: wp('58%'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  textbuttonchart: {
    fontSize: 16,
    fontFamily: 'Poppins-Bold',
    color: 'white',
  },
  line: {
    height: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
    width: hp('100%'),
  },
});
