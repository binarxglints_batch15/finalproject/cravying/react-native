import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {Rating} from 'react-native-ratings';
import HeaderProfile from '../components/Header/HeaderProfile';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const STAR_IMAGE = require('../assets/image/star.png');
// ratingCompleted = rating => {
//   console.log('Rating is: ' + rating);
// };

const Profile = props => {
  return (
    <View style={{flex: 1}}>
      <HeaderProfile title="Profile" iconright="logout-variant" />
      <View style={styles.contain}>
        <View style={styles.main}>
          <Text style={styles.maintxt}>Hai Rara!</Text>
          <Text style={styles.maintxt1}>
            Your rating helps us improve our ability to provide you with the
            best service possible.
          </Text>
        </View>
        <View style={styles.rating}>
          <Rating
            type="custom"
            ratingImage={STAR_IMAGE}
            ratingColor="#f1c40f"
            ratingBackgroundColor="#c8c7c8"
            ratingCount={5}
            showRating={false}
            startingValue={0}
            imageSize={40}
            onFinishRating={console.log('rating completed')}
          />
        </View>
        <View style={styles.mid}>
          <TouchableOpacity
            onPress={() => props.navigation.navigate('Profile2')}
            style={styles.button}>
            <Text style={styles.buttontxt}>Submit</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default Profile;

const styles = StyleSheet.create({
  contain: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  main: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    paddingVertical: 30,
    paddingHorizontal: 20,
  },
  maintxt: {
    fontFamily: 'Poppins-Bold',
    fontSize: 24,
    color: '#313440',
  },
  maintxt1: {
    fontFamily: 'Poppins-Reguler',
    fontSize: 14,
    color: '#313440',
  },
  rating: {
    alignSelf: 'center',
    marginVertical: 20,
  },
  mid: {
    justifyContent: 'center',
    alignSelf: 'center',
  },
  button: {
    borderWidth: 1,
    borderColor: '#FF5353',
    borderRadius: 10,
    backgroundColor: '#FFFFFF',
    width: wp('90%'),
    height: hp('7%'),
    justifyContent: 'center',
  },
  buttontxt: {
    textAlign: 'center',
    fontSize: 16,
    fontFamily: 'Poppins-Reguler',
    color: '#FF5353',
  },
});
