import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  ScrollView,
  StyleSheet,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Header from '../../components/Header/HeaderProfile';
import CardCart from '../../components/card/CardCart';

const Cart = props => {
  const [hapus, setHapus] = useState([
    {
      title: 'Menu A for 1 Pax',
      price: 'Rp. 176.000',
      discount: 'Rp. 220.000',
      orders: {
        order1: '+ Scrambled Egg with Tomato',
        order2: '+ Chicken in Sichuan Chili Oil Sauce',
      },
      note: 'Note',
    },
    {
      title: 'Menu A for 2 Pax',
      price: 'Rp. 150.000',
      discount: 'Rp. 200.000',
      orders: {
        order1: '+ Scrambled Egg with Tomato',
        order2: '+ Chicken Burito With Chilie Sauce',
      },
      note: 'Note',
    },
    {
      title: 'Cucumber Salad',
      price: 'Rp. 88.000',
      discount: 'Rp. 110.000',
      orders: {},
      note: 'Note',
    },
  ]);

  function press(title) {
    // console.log(title);
    const hapusdata = hapus.filter(e => {
      return e.title !== title;
    });
    setHapus(hapusdata);
    // console.log(hapusdata);
  }

  const renderItem = ({item}) => {
    return <CardCart data={item} press={e => press(e)} />;
  };
  return (
    <ScrollView>
      <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
        <Header title="Your Cart" />
        <View style={styles.container}>
          <FlatList data={hapus} renderItem={renderItem} />
          <View style={styles.mid}>
            <View style={styles.itemmid}>
              <Text style={styles.txtmid}>Price</Text>
              <Text style={styles.txtmid}>Rp 0</Text>
            </View>
            <View style={style.itemmid}>
              <Text style={styles.txtmid}>Discount</Text>
              <Text style={styles.txtmid}>-Rp 0</Text>
            </View>
          </View>
          <View style={styles.bot}>
            <Text style={styles.txtbot}>Total payment</Text>
            <Text style={styles.txtbot}>Rp 0</Text>
          </View>
        </View>
        <TouchableOpacity
          onPress={() => props.navigation.navigate('MerchantProfile')}
          style={styles.button}>
          <Text style={styles.buttontxt}>Go To Checkout</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

export default Cart;

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 19,
    marginVertical: 25,
  },
  mid: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    height: hp('15%'),
    paddingVertical: 20,
    borderBottomColor: '#D3D9FF',
    borderBottomWidth: 1,
  },
  itemmid: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  txtmid: {
    fontSize: 14,
    fontFamily: 'ProximaNova-Reguler',
    color: '#313440',
  },
  bot: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: hp('7%'),
    paddingVertical: 18,
  },
  txtbot: {
    fontSize: 14,
    fontFamily: 'Proxima-Nova-Bold',
    color: '#313440',
  },
  button: {
    borderColor: '#C2C4CD',
    borderRadius: 10,
    alignSelf: 'center',
    justifyContent: 'center',
    width: wp('89%'),
    height: hp('7%'),
    marginVertical: 100,
    backgroundColor: '#FF5353',
  },
  buttontxt: {
    fontFamily: 'Poppins-Reguler',
    fontSize: 16,
    color: '#FFFFFF',
    textAlign: 'center',
  },
});
