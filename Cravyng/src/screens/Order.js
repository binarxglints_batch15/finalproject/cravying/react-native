import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Image,
  ScrollView,
  TouchableOpacity,
  StatusBar,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Button from '../components/Button/Button';
import CardCheckout from '../components/card/CardCheckout';
import HeaderProfile from '../components/Header/HeaderProfile';
import CardCheckout1 from '../components/card/CardCheckout1';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {jsxOpeningElement} from '@babel/types';
import {
  NavigationContainerRefContext,
  NavigationHelpersContext,
  useNavigation,
} from '@react-navigation/core';

const Order = props => {
  const data = [
    {
      id: 1,
      PriceOrder: 'Rp 157.600',
      Status: 'Paid',
      DateId: '#927467375',
      Date: '28/03/21',
    },
    {
      id: 2,
      PriceOrder: 'Rp 157.600',
      Status: 'Paid',
      DateId: '#927467375',
      Date: '28/03/21',
    },
    {
      id: 3,
      PriceOrder: 'Rp 157.600',
      Status: 'Paid',
      DateId: '#927467375',
      Date: '28/03/21',
    },
    {
      id: 4,
      PriceOrder: 'Rp 157.600',
      Status: 'Paid',
      DateId: '#927467375',
      Date: '28/03/21',
    },
    {
      id: 5,
      PriceOrder: 'Rp 157.600',
      Status: 'Paid',
      DateId: '#927467375',
      Date: '28/03/21',
    },
    {
      id: 6,
      PriceOrder: 'Rp 157.600',
      Status: 'Unpaid',
      DateId: '#927467375',
      Date: '28/03/21',
    },
    {
      id: 7,
      PriceOrder: 'Rp 157.600',
      Status: 'Unpaid',
      DateId: '#927467375',
      Date: '28/03/21',
    },
  ];

  const checkout = [
    {
      id: '1',
      namemenu: 'Menu A for 1 Pax',
      price: 'Rp 176.000',
      pricedis: 'Rp 220.000',
      toping: {
        toping1: '+ Scrambled Egg with Tomato',
        toping2: '+ Chicken in Sichuan Chili Oil Sauce',
      },
      notes: 'no mayo',
    },
    {
      id: '2',
      namemenu: 'Chongqing Spicy Chick...',
      price: 'Rp 176.000',
      pricedis: 'Rp 220.000',
      toping: {},
      notes: '-',
    },
  ];
  // const [role, setRole] = useState('user');
  const renderCard = ({item}) => {
    return <CardCheckout data={item} />;
  };

  const renderItem = ({item}) => {
    // console.log('INI DARI RENDER ITEM', item);
    return <CardCheckout1 data={item} />;
  };

  const navigation = useNavigation();
  const role = 'user';
  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <StatusBar
        translucent={true}
        backgroundColor={'transparent'}
        barStyle="dark-content"
      />
      <HeaderProfile
        title={role == 'merchant' ? 'Order History' : 'My Order'}
        title2={role == 'merchant' ? '' : '#927467375'}
      />
      <View style={{flex: 1}}>
        {role == 'merchant' && data ? (
          <>
            <FlatList
              data={data}
              renderItem={renderCard}
              keyExtractor={(index, i) => i}
            />
          </>
        ) : role == 'user' && data ? (
          <>
            <ScrollView>
              <View style={styles.container}>
                <View style={styles.textrow}>
                  <Icon
                    style={styles.iconstyle}
                    name="storefront"
                    size={15}
                    color="#C2C4CD"
                  />
                  <Text style={styles.textup}>Heavenly Taste</Text>
                </View>
                <View style={styles.textrow2}>
                  <Icon
                    style={styles.iconstyle}
                    name="person"
                    size={15}
                    color="#C2C4CD"
                  />
                  <Text style={styles.textup}>Rara Sekar</Text>
                </View>
              </View>
              <View>
                <FlatList
                  data={checkout}
                  renderItem={renderItem}
                  keyExtractor={(index, i) => i}
                />
              </View>
              <View style={styles.boxmain}>
                <View style={styles.box}>
                  <View style={styles.text}>
                    <Text style={styles.textmain}>Price</Text>
                    <Text style={styles.textmain}>Rp 440.000</Text>
                  </View>
                  <View style={styles.text}>
                    <Text style={styles.textmain}>Discount</Text>
                    <Text style={styles.textmain}>-Rp 88.000</Text>
                  </View>
                  <View style={styles.line}></View>
                  <View style={styles.text}>
                    <Text style={styles.textmain2}>Total Payment</Text>
                    <Text style={styles.textmain2}>Rp 352.000</Text>
                  </View>
                </View>
                <TouchableOpacity
                  style={styles.boxbutton}
                  onPress={() => navigation.navigate('Menu')}>
                  <Text style={styles.buttontext}>Make New Order</Text>
                </TouchableOpacity>
              </View>
            </ScrollView>
          </>
        ) : (
          <>
            <View style={styles.boximage}>
              <Image source={require('../assets/image/orderno.png')}></Image>
              <Text style={styles.text1}>Uh-oh!</Text>
              <Text style={styles.text2}>
                No orders found. Try to order something, maybes?
              </Text>
              <Button name="Find Delicious Food" />
            </View>
          </>
        )}
      </View>
    </View>
  );
};

export default Order;

const styles = StyleSheet.create({
  boximage: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text1: {
    fontSize: 16,
    color: 'black',
    fontFamily: 'Poppins-Bold',
  },
  text2: {
    fontSize: 16,
    color: 'black',
    fontFamily: 'ProximaNova-Regular',
    width: wp('80%'),
    textAlign: 'center',
    marginBottom: 20,
  },
  container: {
    marginHorizontal: 20,
    backgroundColor: 'white',
  },
  iconstyle: {
    marginVertical: 3,
    marginRight: 10,
  },
  boxmain: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  line: {
    height: 1,
    width: wp('90%'),
    backgroundColor: '#D3D9FF',

    marginVertical: 1,
  },
  box: {
    width: '90%',
  },
  text: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 10,
  },
  textrow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
  },
  textrow2: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 35,
  },
  textup: {
    fontSize: 12,
    fontFamily: 'ProximaNova-Regular',
  },
  textmain: {
    fontSize: 14,
    fontFamily: 'Proxima-Nova-Regular',
    color: '#313440',
  },
  textmain2: {
    fontSize: 14,
    fontFamily: 'Proxima-Nova-Bold',
    color: '#313440',
  },
  boxbutton: {
    borderRadius: 10,
    backgroundColor: 'white',
    width: wp('90%'),
    height: hp('7%'),
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'red',
    borderWidth: 1,
    marginVertical: 20,
  },
  buttontext: {
    color: 'red',
  },
});
