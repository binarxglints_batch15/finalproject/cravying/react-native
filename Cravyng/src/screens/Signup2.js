import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
  StatusBar,
} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import Icon from '../components/Icon/Icon';
import Back from '../components/Back/Back';
import Input from '../components/Input/Input';
import InPassword from '../components/InPassword/InPAssword';
import Button from '../components/Button/Button';
import {FloatingLabelInput} from 'react-native-floating-label-input';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useNavigation} from '@react-navigation/core';
import signup from '../redux/redux/signup';
const Signup2 = props => {
  const [resName, setResName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfrimPassword] = useState('');
  const [checkBox, setCheckBox] = useState(false);

  const dataSignup = {
    email,
    password,
    confirmPassword,
  };
  const navigation = useNavigation();
  // console.log(cPassword);
  return (
    <ScrollView style={{backgroundColor: 'white', flex: 1}}>
      <StatusBar translucent={true} backgroundColor={'transparent'} />
      <View style={{flex: 1, marginVertical: 20}}>
        <Back name="Back" navigate="Signin" />
      </View>
      <View style={{flex: 120}}>
        <View style={style.chiken}>
          <Image source={require('../assets/image/chiken.png')} />
        </View>
        <View style={{marginTop: '3%'}}>
          <Text style={style.signup}>Sign up</Text>
        </View>
        <View>
          <Input hint="Email" value={email} onTxt={setEmail} />
        </View>
        <View style={{marginVertical: '2%'}}>
          <InPassword hint="Password" value={password} onTxt={setPassword} />
        </View>
        <View>
          <InPassword
            hint="Confirm Password"
            value={confirmPassword}
            onTxt={setConfrimPassword}
          />
        </View>
        <View style={{marginVertical: '7%'}}>
          <Button
            name="Sign up"
            handleNavigation={() =>
              navigation.navigate('Login', {
                email: email,
                password: password,
                confirmPassword: confirmPassword,
              })
            }
          />
        </View>
      </View>
      <View style={{flexDirection: 'row'}}>
        <CheckBox
          style={{marginLeft: '4%'}}
          disabled={false}
          value={checkBox}
          onValueChange={newValue => setCheckBox(newValue)}
        />
        <Text style={{fontSize: 14, color: '#313440', marginTop: 4}}>
          I agree with Cravyng terms and conditions
        </Text>
      </View>
      <View style={style.or}>
        <Text style={style.txt}>____________________ </Text>
        <Text style={style.txt}>or</Text>
        <Text style={style.txt}> ____________________</Text>
      </View>

      <Icon />
      <View style={style.account}>
        <Text>Allready have an account?</Text>
        <TouchableOpacity onPress={() => props.navigation.navigate('Signin')}>
          <Text style={{color: 'blue'}}> Sign in</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

export default Signup2;

const style = StyleSheet.create({
  or: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: '16%',
  },
  txt: {
    fontSize: 16,
    color: '#868993',
  },
  account: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 16,
  },
  signup: {
    color: '#313440',
    fontFamily: 'Poppins-Bold',
    fontSize: 31,
    marginLeft: '4%',
  },
  chiken: {
    marginTop: '13%',
    marginLeft: '4%',
  },
});
