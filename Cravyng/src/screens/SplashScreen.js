import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  ImageBackground,
  StatusBar,
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const Landingpage = props => {
  // const testing = () =>{
  //   props.navigation.navigate('Splash');

  // }
  return (
    <View>
      <StatusBar backgroundColor="transparent" translucent={true} />
      <ImageBackground
        source={require('../assets/image/bgSplash.png')}
        style={{height: '100%', width: '100%'}}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.6)',
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View style={style.vector}>
            <Image source={require('../assets/image/cravyng.png')} />
          </View>
        </View>
        <View
          style={{
            position: 'absolute',
            bottom: 0,
            left: 20,
          }}>
          <View style={{marginTop: '10%'}}>
            <Text style={style.continue}>Continue to Cravyng as?</Text>
          </View>
          <View style={{alignItems: 'center'}}>
            <TouchableOpacity
              onPress={() => props.navigation.navigate('Signin', {nav: '1'})}
              style={style.costumer}>
              <Image source={require('../assets/image/costumer.png')} />
              <Text style={style.costumerTxt}> Costumer</Text>
            </TouchableOpacity>
          </View>
          <View style={{alignItems: 'center'}}>
            <TouchableOpacity
              onPress={() => props.navigation.navigate('Signin', {nav: '2'})}
              style={style.merchant}>
              <Image source={require('../assets/image/merchant.png')} />
              <Text style={style.merchantTxt}> Merchant</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    </View>
  );
};

export default Landingpage;

const style = StyleSheet.create({
  merchant: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#FF5353',
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    height: hp('8%'),
    width: wp('90%'),
  },
  costumerTxt: {
    color: '#FFFFFF',

    fontSize: 16,
  },
  costumer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 2,
    borderColor: '#FF5353',
    backgroundColor: '#FF5353',
    borderRadius: 10,
    height: hp('8%'),
    width: wp('90%'),
    marginVertical: '3%',
  },
  continue: {
    textAlign: 'center',
    fontFamily: 'Poppins-Regular',
    color: '#FFFFFF',
    fontSize: 16,
    marginTop: '30%',
  },
  vector: {
    alignItems: 'center',
  },
  merchantTxt: {
    color: '#FF5353',

    fontSize: 16,
  },
});
