import React, {useState, useEffect} from 'react';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  StatusBar,
  ScrollView,
  BackHandler,
} from 'react-native';
import Icon from '../components/Icon/Icon';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Input from '../components/Input/Input';
import InPassword from '../components/InPassword/InPAssword';
import Button from '../components/Button/Button';
import SplashScreen2 from './SplashScreen2';
import {connect} from 'react-redux';
import auth from '../redux/redux/auth';
import Modal from 'react-native-modal';
const Signin = props => {
  const [email, setEmail] = useState('customer5@mail.com');
  const [password, setPassword] = useState('Customer5123@');
  const [load, setLoad] = useState(true);
  const [data, setData] = useState(props.route.params?.nav);
  const [isModalVisible2, setModalVisible2] = useState(false);

  const dataLogin = {
    email,
    password,
  };
  useEffect(() => {
    setTimeout(() => {
      setLoad(false);
    }, 2000);
  }, []);

  // useEffect(() => {
  //   if (props.isLoading == false) {
  //     setModalVisible2(true);
  //   } else {
  //     setModalVisible2(false);
  //   }
  // }, [props.isLoading]);
  if (load) {
    return <SplashScreen2></SplashScreen2>;
  }

  return (
    <ScrollView style={{backgroundColor: 'white', flex: 1}}>
      <Modal
        // hasBackdrop={true}
        hasBackdrop={true}
        isVisible={props.isLoading}
        // onBackdropPress={toggleModal2}
        testID={'modal'}
        style={{justifyContent: 'center', alignItems: 'center', maargin: 0}}>
        <View>
          <Image
            style={{borderRadius: 10, width: wp('23'), height: wp('23')}}
            source={require('../assets/image/logo.png')}></Image>
        </View>
      </Modal>

      <StatusBar
        translucent={true}
        backgroundColor={'transparent'}
        barStyle="dark-content"
      />

      <View style={{flex: 1}}>
        <TouchableOpacity onPress={() => props.navigation.navigate('Splash')}>
          <View
            style={{
              flexDirection: 'row',
              height: hp('7%'),
              marginVertical: 20,
            }}>
            <Image
              source={require('../assets/image/back.png')}
              style={style.img}
            />
            <Text style={style.headerTxt}>Back</Text>
          </View>
        </TouchableOpacity>
      </View>
      <View style={{flex: 7}}>
        <View style={style.tacos}>
          <Image source={require('../assets/image/tacos.png')} />
        </View>
        <View style={{marginTop: '4%'}}>
          <Text style={style.signin}>Sign in</Text>
        </View>
        <View style={{marginVertical: '4%'}}>
          <Input hint="Email" value={email} onTxt={setEmail} />
        </View>
        <View>
          <InPassword hint="Password" value={password} onTxt={setPassword} />
        </View>
        <View style={{marginTop: '4%'}}>
          <Button
            name={props.isLoading == false ? 'Sign In' : ' Loading....'}
            handleNavigation={() => props.login(dataLogin, props.navigation)}
          />
        </View>
        <View style={{marginHorizontal: 20}}>
          {props.error ? (
            <Text style={{color: 'red', fontFamily: 'Poppins-Medium'}}>
              ! {props.error}
            </Text>
          ) : null}
        </View>
      </View>
      <View style={{flex: 4}}>
        <View style={style.or}>
          <Text style={style.txt}>____________________ </Text>
          <Text style={style.txt}>or</Text>
          <Text style={style.txt}> ____________________</Text>
        </View>
        <View style={{marginBottom: '15%'}}>
          <Icon />
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginTop: '5%',
          }}>
          <Text>Don't have an account? </Text>
          <TouchableOpacity
            onPress={() =>
              props.navigation.navigate('Signup', {role: 'merchant'})
            }>
            <Text style={{color: 'blue'}}>Sign up as Merchant</Text>
          </TouchableOpacity>
          <Text> or</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginBottom: '10%',
          }}>
          <TouchableOpacity
            onPress={() => props.navigation.navigate('Signup2')}>
            <Text style={{color: 'blue'}}>Sign up as Costumer</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

const reduxDispatchlogin = dispatch => ({
  login: (dataLogin, n) =>
    dispatch({type: 'LOGIN_START', data: dataLogin, navigasi: n}),
});

const reduxState = state => ({
  isLoading: state.auth.isLoading,
  error: state.auth.error,
});
export default connect(reduxState, reduxDispatchlogin)(Signin);

const style = StyleSheet.create({
  or: {
    flexDirection: 'row',
    justifyContent: 'center',
    // marginVertical: 30,
  },
  txt: {
    fontSize: 16,
    color: '#868993',
  },
  signin: {
    marginHorizontal: '4%',
    fontFamily: 'Poppins-Bold',
    fontSize: 31,
    color: 'black',
  },
  tacos: {
    marginTop: '10%',
    marginLeft: '4%',
  },
  img: {
    marginTop: 27,
    marginLeft: 24,
  },
  headerTxt: {
    marginLeft: 13,
    marginTop: 22,
    fontSize: 16,
    color: 'black',
  },
});
