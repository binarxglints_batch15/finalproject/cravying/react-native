import React, {useState, useRef, useEffect} from 'react';
import {
  ImageBackground,
  StyleSheet,
  ScrollView,
  View,
  Animated,
  Image,
  Text,
  Pressable,
  TouchableOpacity,
  FlatList,
  StatusBar,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import HeaderProfile from '../components/Header/HeaderProfile';
import CardDiskon from '../components/card/cardDiskon';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Icon2 from 'react-native-vector-icons/AntDesign';
import CardMenu from '../components/card/cardMenu';
import {useNavigation} from '@react-navigation/core';
import Modal from 'react-native-modal';
import Star from 'react-native-vector-icons/MaterialCommunityIcons';
import {connect} from 'react-redux';

const Menu = props => {
  const [headerShown, setHeaderShown] = useState(false);
  const translation = useRef(new Animated.Value(-100)).current;
  const [isModalVisible, setModalVisible] = useState(false);
  const [isModalVisible2, setModalVisible2] = useState(false);
  const [ref, setref] = useState(null);
  const [Value, setValue] = useState('Recomended');
  const navigation = useNavigation();

  const toggleModal = props => {
    setModalVisible(!isModalVisible);
  };
  const toggleModal2 = props => {
    setModalVisible2(props);
  };

  useEffect(() => {
    if (props.route.params?.action) {
      toggleModal2(true);
      setTimeout(() => {
        toggleModal2(false);
      }, 3000);
    } else {
    }
  }, [props.route.params]);

  useEffect(() => {
    props.getDataMenu();
  }, []);

  const data = [
    {
      id: 1,
      menu: 'Recomended',
      menu1: 'Menu A for 1 Pax',
      price1: 'Rp 176.000',
      pricediscount: 'Rp 220.000',
      imagemenu: require('../assets/image/gambar.png'),
      icon: 'recommend',
      icon2: 'tago',
    },

    {
      id: 2,
      menu: 'Recomended',
      menu1: 'Menu A for 2 Passs',
      icon: 'heart',
      price1: 'Rp 176.000',
      pricediscount: 'Rp 220.000',
      imagemenu: require('../assets/image/gambar2.png'),
      icon: 'recommend',
      icon2: 'tago',
    },
    {
      id: 3,
      menu: 'Recomended',
      icon: 'heart',
      menu1: 'Menu A for 3 Paxssss',
      price1: 'Rp 176.000',
      pricediscount: 'Rp 220.000',
      imagemenu: require('../assets/image/pictureheader.png'),
      icon: 'recommend',
      icon2: 'tago',
    },
    {
      id: 4,
      menu: 'Recomended',
      menu1: 'Menu A for 4 Pax',
      icon: 'heart',
      price1: 'Rp 176.000',
      pricediscount: 'Rp 220.000',
      imagemenu: require('../assets/image/gambar4.png'),
      icon: 'recommend',
      icon2: 'tago',
    },
  ];

  const data2 = [
    {
      id: 11,
      menu: 'Favorite',
      menu1: 'Menu A for 1 Pax',
      price1: 'Rp 176.000',
      pricediscount: 'Rp 220.000',
      imagemenu: require('../assets/image/gambar.png'),
      icon2: 'tago',
    },

    {
      id: 12,
      menu: 'Favorite',
      menu1: 'Menu A for 1 Pax',
      price1: 'Rp 176.000',
      pricediscount: 'Rp 220.000',
      imagemenu: require('../assets/image/gambar.png'),
    },
    {
      id: 13,
      menu: 'Favorite',
      menu1: 'Menu A for 1 Pax',
      price1: 'Rp 176.000',
      pricediscount: 'Rp 220.000',
      imagemenu: require('../assets/image/gambar.png'),
    },
    {
      id: 14,
      menu: 'Favorite',
      menu1: 'Menu A for 1 Pax',
      price1: 'Rp 176.000',
      pricediscount: 'Rp 220.000',
      imagemenu: require('../assets/image/gambar.png'),
    },
  ];

  const data4 = [
    {
      id: 21,
      menu: 'Favorite',
      menu1: 'Menu A for 1 Pax',
      price1: 'Rp 176.000',
      pricediscount: 'Rp 220.000',
      imagemenu: require('../assets/image/gambar.png'),
    },

    {
      id: 22,
      menu: 'Favorite',
      menu1: 'Menu A for 1 Pax',
      price1: 'Rp 176.000',
      pricediscount: 'Rp 220.000',
      imagemenu: require('../assets/image/gambar.png'),
    },
    {
      id: 23,
      menu: 'Favorite',
      menu1: 'Menu A for 1 Pax',
      price1: 'Rp 176.000',
      pricediscount: 'Rp 220.000',
      imagemenu: require('../assets/image/gambar.png'),
    },
    {
      id: 24,
      menu: 'Favorite',
      menu1: 'Menu A for 1 Pax',
      price1: 'Rp 176.000',
      pricediscount: 'Rp 220.000',
      imagemenu: require('../assets/image/gambar.png'),
    },
    {
      id: 25,
      menu: 'Favorite',
      menu1: 'Menu A for 1 Pax',
      price1: 'Rp 176.000',
      pricediscount: 'Rp 220.000',
      imagemenu: require('../assets/image/gambar.png'),
    },
    {
      id: 26,
      menu: 'Favorite',
      menu1: 'Menu A for 1 Pax',
      price1: 'Rp 176.000',
      pricediscount: 'Rp 220.000',
      imagemenu: require('../assets/image/gambar.png'),
    },
    {
      id: 27,
      menu: 'Favorite',
      menu1: 'Menu A for 1 Pax',
      price1: 'Rp 176.000',
      pricediscount: 'Rp 220.000',
      imagemenu: require('../assets/image/gambar.png'),
    },
    {
      id: 28,
      menu: 'Favorite',
      menu1: 'Menu A for 1 Pax',
      price1: 'Rp 176.000',
      pricediscount: 'Rp 220.000',
      imagemenu: require('../assets/image/gambar.png'),
    },
    {
      id: 29,
      menu: 'Favorite',
      menu1: 'Menu A for 1 Pax',
      price1: 'Rp 176.000',
      pricediscount: 'Rp 220.000',
      imagemenu: require('../assets/image/gambar.png'),
    },
    {
      id: 30,
      menu: 'Favorite',
      menu1: 'Menu A for 1 Pax',
      price1: 'Rp 176.000',
      pricediscount: 'Rp 220.000',
      imagemenu: require('../assets/image/gambar.png'),
    },
    {
      id: 31,
      menu: 'Favorite',
      menu1: 'Menu A for 1 Pax',
      price1: 'Rp 176.000',
      pricediscount: 'Rp 220.000',
      imagemenu: require('../assets/image/gambar.png'),
    },
    {
      id: 32,
      menu: 'Favorite',
      menu1: 'Menu A for 1 Pax',
      price1: 'Rp 176.000',
      pricediscount: 'Rp 220.000',
      imagemenu: require('../assets/image/gambar.png'),
    },
    {
      id: 33,
      menu: 'Favorite',
      menu1: 'Menu A for 1 Pax',
      price1: 'Rp 176.000',
      pricediscount: 'Rp 220.000',
      imagemenu: require('../assets/image/gambar.png'),
    },
  ];

  const data3 = [
    {
      category: 'Recommended',
    },

    {
      category: 'Most Favorite',
    },
    {
      category: 'Appetizers',
    },
    {
      category: 'Hot Dishes',
    },
    {
      category: 'Seafood Dishes and Others',
    },
    {
      category: 'Vegetable Dishes',
    },
    {
      category: 'Staple Food',
    },
    {
      category: 'Fried Dishes',
    },
    {
      category: 'Desserts',
    },
    {
      category: 'Soup',
    },
    {
      category: 'Beverages',
    },
  ];

  // console.log(headerShown);

  console.log(props.role);

  const renderCard = ({item}) => {
    return <CardMenu data={item} passing={props.route.params?.action} />;
  };

  console.log(props.dataMenu);
  return (
    <>
      <StatusBar translucent={true} backgroundColor={'transparent'} />
      <Animated.View
        style={{
          position: 'absolute',
          top: 0,
          left: 0,
          right: 0,
          height: hp('24%'),
          backgroundColor: 'white',
          //The last number on headerShow mean the number or position when start
          transform: [{translateY: headerShown ? hp('24%') : 0}],
        }}>
        <View style={styles.container}>
          <ImageBackground
            style={styles.bgimage}
            source={require('../assets/image/pictureheader.png')}>
            <View style={styles.box}>
              <Text style={styles.textHead}>Heavenly Taste</Text>
              <View style={styles.textbot}>
                <Star
                  name="star-circle"
                  size={16}
                  color={'white'}
                  style={styles.star}
                />
                <Text style={styles.text2}>4.7</Text>
              </View>
            </View>
          </ImageBackground>
          <View style={{position: 'absolute', top: hp('21%'), zIndex: 99}}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}>
              <View style={{flexDirection: 'row', marginLeft: 20}}>
                <CardDiskon disone="20%" dis="20%" rupiah="Rp.300.000" />
                <CardDiskon disone="10%" dis="10%" rupiah="Rp.170.000" />
              </View>
            </ScrollView>
          </View>
          <View style={styles.boxoptions}>
            <TouchableOpacity onPress={() => toggleModal()}>
              <View style={styles.boxoout}>
                <Text>{Value}</Text>
                <Icon name="keyboard-arrow-down" size={25} color="grey" />
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Animated.View>

      <Animated.View
        style={{
          position: 'absolute',
          top: -200,
          left: 0,
          right: 0,
          height: 200,
          backgroundColor: 'white',
          //The last number on headerShow mean the number or position when start
          transform: [{translateY: headerShown ? 200 : 0}],
        }}>
        <View>
          <HeaderProfile title="Heavelny test" />
          <View style={styles.boxoptions2}>
            <Pressable onPress={() => toggleModal()}>
              <View style={styles.boxoout}>
                <Text>{Value}</Text>
                <Icon name="keyboard-arrow-down" size={25} color="grey" />
              </View>
            </Pressable>
          </View>
        </View>
      </Animated.View>

      {/* MODALLLL */}
      <StatusBar
        translucent
        backgroundColor="transparent"
        barStyle={headerShown ? 'dark-content' : 'light-content'}
      />
      <Modal
        hasBackdrop={true}
        isVisible={isModalVisible}
        testID={'modal'}
        style={styles.view}>
        <View
          style={{
            width: wp('100%'),
            height: hp('81%'),
            backgroundColor: 'white',
            justifyContent: 'space-evenly',
            borderRadius: 16,
            borderBottomEndRadius: 0,
            borderBottomLeftRadius: 0,
          }}>
          <TouchableOpacity>
            <Text
              style={{
                fontSize: 14,
                color: 'black',
                marginHorizontal: 10,
                marginVertical: 15,
                fontWeight: '600',
              }}>
              Menu
            </Text>
          </TouchableOpacity>
          {data3.map(item => (
            <>
              <View style={styles.border}></View>
              <Text
                style={styles.textmodal}
                onPress={() => {
                  setValue(item.category), toggleModal(false);
                }}>
                {item.category}
              </Text>
            </>
          ))}
        </View>
      </Modal>

      {/* MODAL FOR VIEW CHART */}
      <Modal
        // hasBackdrop={true}
        hasBackdrop={false}
        isVisible={isModalVisible2}
        onBackdropPress={toggleModal2}
        testID={'modal'}
        style={styles.view}>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            bottom: 75,
            borderRadius: 10,
          }}>
          <View
            style={{
              backgroundColor: '#328E8E',
              height: hp('8%'),
              width: wp('91%'),
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text style={{color: 'white', fontSize: 14, marginHorizontal: 20}}>
              {props.route.params?.action} item add to chart
            </Text>
            <TouchableOpacity onPress={() => navigation.navigate('Cart')}>
              <Text
                style={{color: 'white', fontSize: 14, marginHorizontal: 20}}>
                VIEW
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>

      <ScrollView
        ref={ref => {
          setref(ref);
        }}
        style={{
          flex: 1,
          backgroundColor: 'red',
        }}
        onScroll={event => {
          const scrolling = event.nativeEvent.contentOffset.y;

          if (scrolling > 100) {
            setHeaderShown(true);
          } else {
            setHeaderShown(false);
          }
        }}
        scrollEventThrottle={16}
        style={headerShown ? {marginTop: 160} : {marginTop: 290}}
        // onScroll will be fired every 16ms
      >
        <View style={{flex: 1, backgroundColor: 'white'}}>
          <View style={styles.boxmenu}>
            <Icon2 name="like1" size={20} color="#FF5353" />
            <Text style={styles.text}> Recommended</Text>
          </View>
          <View style={styles.boxflat}>
            <FlatList
              data={data}
              numColumns={2}
              renderItem={renderCard}
              keyExtractor={(index, i) => i}
            />
          </View>
          <View style={styles.boxmenu}>
            <Icon2 name="like1" size={20} color="#FF5353" />
            <Text style={styles.text}> Most favorite</Text>
          </View>
          <FlatList
            data={data2}
            numColumns={2}
            renderItem={renderCard}
            keyExtractor={(index, i) => i}
          />
          <View style={styles.boxmenu3}>
            <Text style={styles.text}> Most favorite</Text>
          </View>
          <View>
            <FlatList
              data={data4}
              numColumns={2}
              renderItem={renderCard}
              keyExtractor={(index, i) => i}
            />
          </View>
        </View>
      </ScrollView>
    </>
  );
};

const reduxDispatchMenu = dispatch => ({
  getDataMenu: () => dispatch({type: 'GETDATA_START'}),
});

const reduxState = state => ({
  role: state.auth.role,
  dataMenu: state.menucat.dataMenu,
});

export default connect(reduxState, reduxDispatchMenu)(Menu);
const styles = StyleSheet.create({
  iconimg: {
    height: hp('2%'),
    width: wp('4%'),
    resizeMode: 'cover',
  },
  container: {
    flex: 1,
    position: 'absolute',
  },
  bgimage: {
    height: hp('25%'),
    width: wp('100%'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  textHead: {
    fontSize: 24,
    fontFamily: 'Poppins-Bold',
    color: 'white',
  },
  box: {
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
    height: hp('25%'),
    width: wp('100%'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  textbot: {
    flexDirection: 'row',

    justifyContent: 'center',
    alignItems: 'center',
  },
  text2: {
    fontSize: 16,
    color: 'white',
    fontFamily: 'Poppins-Medium',
    marginHorizontal: 5,
  },
  boxout1: {flex: 1},
  boxoout: {
    backgroundColor: '#F5F7FA',
    width: wp('85%'),
    height: hp('5%'),
    flexDirection: 'row',
    borderRadius: 10,

    justifyContent: 'space-between',

    alignItems: 'center',
    paddingHorizontal: 20,
  },
  boxoptions: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    height: hp('14%'),
    marginTop: 16,
  },
  boxoptions2: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    height: hp('7%'),
    marginVertical: 10,
  },
  boxmenu: {
    marginHorizontal: 20,
    marginTop: 20,
    flexDirection: 'row',
  },
  text: {
    color: 'black',
    fontSize: 16,
    marginHorizontal: 5,
    fontFamily: 'Poppins-Medium',
  },
  menu: {
    width: wp('95%'),

    flexDirection: 'row',
  },
  boxmenu1: {
    alignItems: 'center',
  },
  boxmenu2: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  boxmenu3: {
    marginHorizontal: 10,
    marginTop: 30,
    flexDirection: 'row',
  },
  view: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  textmodal: {
    lineHeight: 21,
    fontSize: 14,
    color: 'black',
    marginHorizontal: 10,
  },
  border: {
    height: 1,
    backgroundColor: '#D3D9FF',
    width: hp('100%'),
  },
  star: {
    marginRight: 5,
    marginBottom: 4.5,
  },
  boxflat: {
    marginBottom: 30,
  },
});
