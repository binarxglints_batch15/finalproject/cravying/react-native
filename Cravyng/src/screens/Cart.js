import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
  FlatList,
  ScrollView,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Header from '../components/Header/HeaderProfile';
import CardCart from '../components/card/CardCart';
const dataorder = true;

const Cart = props => {
  const [hapus, setHapus] = useState([
    {
      title: 'Menu A for 1 Pax',
      price: 'Rp. 176.000',
      discount: 'Rp. 220.000',
      orders: {
        order1: '+ Scrambled Egg with Tomato',
        order2: '+ Chicken in Sichuan Chili Oil Sauce',
      },
      note: 'Note',
    },
    {
      title: 'Menu A for 2 Pax',
      price: 'Rp. 150.000',
      discount: 'Rp. 200.000',
      orders: {
        order1: '+ Scrambled Egg with Tomato',
        order2: '+ Chicken Burito With Chilie Sauce',
      },
      note: 'Note',
    },
    {
      title: 'Cucumber Salad',
      price: 'Rp. 88.000',
      discount: 'Rp. 110.000',
      orders: {},
      note: 'Note',
    },
  ]);

  function press(title) {
    // console.log(title);
    const hapusdata = hapus.filter(e => {
      return e.title !== title;
    });
    setHapus(hapusdata);
    // console.log(hapusdata);
  }
  const renderItem = ({item}) => {
    return <CardCart data={item} press={e => press(e)} />;
  };
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <Header title="Your Cart" />
      {dataorder ? (
        <>
          <ScrollView>
            <View style={styles.container}>
              <FlatList data={hapus} renderItem={renderItem} />
              <View style={styles.mid}>
                <View style={styles.itemmid}>
                  <Text style={styles.txtmid}>Price</Text>
                  <Text style={styles.txtmid}>Rp 0</Text>
                </View>
                <View style={style.itemmid}>
                  <Text style={styles.txtmid}>Discount</Text>
                  <Text style={styles.txtmid}>-Rp 0</Text>
                </View>
              </View>
              <View style={styles.bot}>
                <Text style={styles.txtbot}>Total payment</Text>
                <Text style={styles.txtbot}>Rp 0</Text>
              </View>
            </View>
            <View>
              <TouchableOpacity
                onPress={() => props.navigation.navigate('Checkout')}
                style={styles.button}>
                <Text style={styles.buttontxt}>Go To Checkout</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </>
      ) : (
        <>
          <View style={style.container}>
            <View style={style.top}>
              <Text style={style.txttop}>0 item(s)</Text>
            </View>
            <View style={style.mid}>
              <View style={style.itemmid}>
                <Text style={style.txtmid}>Price</Text>
                <Text style={style.txtmid}>Rp 0</Text>
              </View>
              <View style={style.itemmid}>
                <Text style={style.txtmid}>Discount</Text>
                <Text style={style.txtmid}>-Rp 0</Text>
              </View>
            </View>
            <View style={style.bot}>
              <Text style={style.txtbot}>Total payment</Text>
              <Text style={style.txtbot}>Rp 0</Text>
            </View>
          </View>

          <TouchableOpacity
            onPress={() => props.navigation.navigate('Cart1b')}
            style={style.button}>
            <Text style={style.buttontxt}>Go To Checkout</Text>
          </TouchableOpacity>
        </>
      )}
    </View>
  );
};

export default Cart;

const style = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 19,
    marginVertical: 24,
  },
  top: {
    alignSelf: 'flex-start',
    borderBottomColor: '#D3D9FF',
    borderBottomWidth: 1,
    width: '100%',
    height: hp('7%'),
  },
  txttop: {
    fontFamily: 'ProximaNova-Reguler',
    fontSize: 14,
    color: '#868993',
  },
  mid: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    height: hp('13%'),
    paddingVertical: 15,
    borderBottomColor: '#D3D9FF',
    borderBottomWidth: 1,
  },
  itemmid: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  txtmid: {
    fontSize: 14,
    fontFamily: 'ProximaNova-Reguler',
    color: '#313440',
  },
  bot: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: hp('7%'),
    marginVertical: 10,
  },
  txtbot: {
    fontSize: 14,
    fontFamily: 'Proxima-Nova-Bold',
    color: '#313440',
  },
  button: {
    borderRadius: 10,
    alignSelf: 'center',
    justifyContent: 'center',
    width: wp('89%'),
    height: hp('7%'),
    marginVertical: 21,
    backgroundColor: '#C2C4CD',
  },
  buttontxt: {
    fontFamily: 'Poppins-Reguler',
    fontSize: 16,
    color: '#FFFFFF',
    textAlign: 'center',
  },
});

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 19,
    marginVertical: 25,
  },
  mid: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    height: hp('15%'),
    paddingVertical: 20,
    borderBottomColor: '#D3D9FF',
    borderBottomWidth: 1,
  },
  itemmid: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  txtmid: {
    fontSize: 14,
    fontFamily: 'ProximaNova-Reguler',
    color: '#313440',
  },
  bot: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: hp('5%'),
    marginVertical: 10,
  },
  txtbot: {
    fontSize: 14,
    fontFamily: 'Proxima-Nova-Bold',
    color: '#313440',
  },
  button: {
    borderColor: '#C2C4CD',
    borderRadius: 10,
    alignSelf: 'center',
    justifyContent: 'center',
    width: wp('89%'),
    height: hp('7%'),
    marginVertical: 20,
    backgroundColor: '#FF5353',
  },
  buttontxt: {
    fontFamily: 'Poppins-Reguler',
    fontSize: 16,
    color: '#FFFFFF',
    textAlign: 'center',
  },
});
