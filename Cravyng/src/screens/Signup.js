import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import Icon from '../components/Icon/Icon';
import Back from '../components/Back/Back';
import Input from '../components/Input/Input';
import InPassword from '../components/InPassword/InPAssword';
import Button from '../components/Button/Button';
import {FloatingLabelInput} from 'react-native-floating-label-input';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Signin from './Signin';
import {connect} from 'react-redux';
const Signup = props => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfrimPassword] = useState('');
  const [checkBox, setCheckBox] = useState(false);
  const [role, setRole] = useState(props.route.params.role);

  const dataSignup = {
    name,
    email,
    password,
    confirmPassword,
    role,
  };
  return (
    <ScrollView style={{backgroundColor: 'white', flex: 1}}>
      <View
        style={{
          height: hp('7%'),
          marginVertical: 20,
          flex: 1,
        }}>
        <View>
          <Back name="Back" navigate="Signin" />
        </View>
      </View>
      <View style={{flex: 11}}>
        <View style={style.chiken}>
          <Image source={require('../assets/image/chiken.png')} />
        </View>
        <View style={{marginTop: '2%'}}>
          <Text style={style.signup}>Sign up</Text>
        </View>
        <View style={{marginVertical: '2%'}}>
          <Input hint="Restaurant Name" value={name} onTxt={setName} />
        </View>
        <View>
          <Input hint="Email" value={email} onTxt={setEmail} />
        </View>
        <View style={{marginVertical: '4%'}}>
          <InPassword hint="Password" value={password} onTxt={setPassword} />
        </View>
        <View>
          <InPassword
            hint="Confirm Password"
            value={confirmPassword}
            onTxt={setConfrimPassword}
          />
        </View>
        <View>
          <Button
            name="Sign up"
            handleNavigation={() => props.signup(dataSignup, props.navigation)} //sddfdfdfdfs
          />
        </View>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <CheckBox
            style={{marginLeft: '4%'}}
            disabled={false}
            value={checkBox}
            onValueChange={newValue => setCheckBox(newValue)}
          />
          <Text style={{fontSize: 12, color: '#313440'}}>
            I agree with Cravyng terms and conditions
          </Text>
        </View>
      </View>
      <View style={{flex: 3}}>
        <View style={style.or}>
          <Text style={style.txt}>____________________ </Text>
          <Text style={style.txt}>or</Text>
          <Text style={style.txt}> ____________________</Text>
        </View>
        <Icon />
        <View style={style.account}>
          <Text>Allready have an account?</Text>
          <TouchableOpacity onPress={() => props.navigation.navigate('Signin')}>
            <Text style={{color: 'blue'}}> Sign in</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};
const reduxDispatchSignup = dispacth => ({
  signup: (dataSignupMerchant, navi) =>
    dispacth({type: 'SIGNUP_START', data: dataSignupMerchant, navigasi: navi}),
});

export default connect(null, reduxDispatchSignup)(Signup);

const style = StyleSheet.create({
  txt: {
    fontSize: 16,
    color: '#868993',
  },
  or: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  account: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 17,
  },
  signup: {
    color: '#313440',
    fontFamily: 'Poppins-Bold',
    fontSize: 27,
    marginLeft: '4%',
  },
  chiken: {
    // marginTop: '6%',
    marginLeft: '4%',
  },
});
