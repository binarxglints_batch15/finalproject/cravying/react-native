import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ActivityIndicator,
  FlatList,
  Image,
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

const Selecteditem = () => {
  const [isLoading, setisLoading] = useState(true);
  const [data, setdata] = useState([]);
  const [currentIndex, setcurrentIndex] = useState();
  const [refFlatlist, setrefFlatlist] = useState();

  useEffect(() => {
    getListPhoto();
    return () => {};
  }, []);

  const getListPhoto = () => {
    const apiURL =
      'https://jsonplaceholder.typicode.com/photos?_Limit=208_page=1';
    fetch(apiURL)
      .then(res => res.json())
      .then(resJson => {
        setdata(resJson);
      })
      .catch(error => {
        console.log('Error: ', error);
      })
      .finally(() => setisLoading(false));
  };

  const onClickItem = (item, index) => {
    setcurrentIndex(index);
    const newArrData = data.map((e, index) => {
      if (item.id == e.id) {
        return {
          ...e,
          selected: true,
        };
      }
      return {
        ...e,
        selected: false,
      };
    });
    setdata(newArrData);
  };
  const renderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => onClickItem(item, index)}
        style={[
          styles.item,
          {
            marginTop: 11,
            height: 150,
            backgroundColor: item.selected ? 'orange' : 'white',
          },
        ]}>
        <Image
          style={styles.image}
          source={{uri: item.url}}
          rezieMode="contain"
        />
      </TouchableOpacity>
    );
  };

  const onScrollToItemSelected = () => {
    refFlatlist.scrollToIndex({animated: true, index: currentIndex});
  };
  const getItemLayout = (data, index) => {
    return {length: 161, offset: 161 * index, index};
  };

  return (
    <View style={styles.container}>
      {isLoading ? (
        <ActivityIndicator />
      ) : (
        <FlatList
          data={data}
          renderItem={renderItem}
          keyExtractor={item => `key-${item.id}`}
          getItemLayout={getItemLayout}
          ref={ref => setrefFlatlist(ref)}
        />
      )}

      <TouchableOpacity onPress={onScrollToItemSelected}>
        <View styles={styles.wrapButton}>
          <Text style={styles.txtFontsize}> Scroll To Item Selected</Text>
        </View>
      </TouchableOpacity>
      <Text></Text>
    </View>
  );
};

export default Selecteditem;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  wrapButton: {
    justifyContent: 'center',
    marginHorizontal: 50,
    padding: 20,
    backgroundColor: 'red',
  },
  txtFontsize: {
    fontSize: 20,
  },
  item: {
    borderWidth: 0.5,
    padding: 8,
    borderRadius: 10,
    justifyContent: 'center',
  },
  image: {
    width: 100,
    height: 100,
  },
});
