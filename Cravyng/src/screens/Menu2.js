import React, {useState} from 'react';
import {
  View,
  Text,
  StatusBar,
  Image,
  StyleSheet,
  ScrollView,
  FlatList,
  TouchableOpacity,
  Pressable,
  Button,
  Animated,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Icon2 from 'react-native-vector-icons/AntDesign';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import CardDiskon from '../components/card/cardDiskon';
import HeaderMenu from '../components/Header/HeaderMenu';
import CardMenu from '../components/card/cardMenu';
import Modal from 'react-native-modal';
import {useNavigation} from '@react-navigation/core';
import ReactNativeParallaxHeader from 'react-native-parallax-header';
const Menu2 = () => {
  const [headershown, setHeadershown] = useState(false);
  const data = [
    {
      menu: 'Recomended',
      menu1: 'Menu A for 1 Pax',
      price1: 'Rp 176.000',
      pricediscount: 'Rp 220.000',
      imagemenu: require('../assets/image/gambar.png'),
    },

    {
      menu: 'Recomended',
      menu1: 'Menu A for 1 Pax',
      price1: 'Rp 176.000',
      pricediscount: 'Rp 220.000',
      imagemenu: require('../assets/image/gambar.png'),
    },
    {
      menu: 'Recomended',
      menu1: 'Menu A for 1 Pax',
      price1: 'Rp 176.000',
      pricediscount: 'Rp 220.000',
      imagemenu: require('../assets/image/gambar.png'),
    },
    {
      menu: 'Recomended',
      menu1: 'Menu A for 1 Pax',
      price1: 'Rp 176.000',
      pricediscount: 'Rp 220.000',
      imagemenu: require('../assets/image/gambar.png'),
    },
  ];

  const data2 = [
    {
      menu: 'Favorite',
      menu1: 'Menu A for 1 Pax',
      price1: 'Rp 176.000',
      pricediscount: 'Rp 220.000',
      imagemenu: require('../assets/image/gambar.png'),
    },

    {
      menu: 'Favorite',
      menu1: 'Menu A for 1 Pax',
      price1: 'Rp 176.000',
      pricediscount: 'Rp 220.000',
      imagemenu: require('../assets/image/gambar.png'),
    },
    {
      menu: 'Favorite',
      menu1: 'Menu A for 1 Pax',
      price1: 'Rp 176.000',
      pricediscount: 'Rp 220.000',
      imagemenu: require('../assets/image/gambar.png'),
    },
    {
      menu: 'Favorite',
      menu1: 'Menu A for 1 Pax',
      price1: 'Rp 176.000',
      pricediscount: 'Rp 220.000',
      imagemenu: require('../assets/image/gambar.png'),
    },
  ];

  const data3 = [
    {
      category: 'Recommended',
    },

    {
      category: 'Most Favorite',
    },
    {
      category: 'Appetizers',
    },
    {
      category: 'Hot Dishes',
    },
    {
      category: 'Seafood Dishes and Others',
    },
    {
      category: 'Vegetable Dishes',
    },
    {
      category: 'Staple Food',
    },
    {
      category: 'Fried Dishes',
    },
    {
      category: 'Desserts',
    },
    {
      category: 'Soup',
    },
    {
      category: 'Beverages',
    },
  ];

  const [isModalVisible, setModalVisible] = useState(false);
  const [Value, setValue] = useState('Recomended');

  const toggleModal = props => {
    setModalVisible(!isModalVisible);
  };
  const navigation = useNavigation();
  return (
    <View style={{flex: 1}}>
      {/* MODALLLL */}
      <StatusBar translucent backgroundColor="transparent" />
      <Modal
        hasBackdrop={true}
        isVisible={isModalVisible}
        testID={'modal'}
        style={style.view}>
        <View
          style={{
            width: wp('100%'),
            height: hp('81%'),
            backgroundColor: 'white',
            justifyContent: 'space-evenly',
            borderRadius: 16,
            borderBottomEndRadius: 0,
            borderBottomLeftRadius: 0,
          }}>
          <TouchableOpacity>
            <Text
              style={{
                fontSize: 14,
                color: 'black',
                marginHorizontal: 10,
                marginVertical: 15,
                fontWeight: '600',
              }}>
              Menu
            </Text>
          </TouchableOpacity>
          {data3.map(item => (
            <>
              <View style={style.border}></View>
              <Text
                style={style.textmodal}
                onPress={() => {
                  setValue(item.category), toggleModal(false);
                }}>
                {item.category}
              </Text>
            </>
          ))}
        </View>
      </Modal>
      {/* MODALLL */}

      <View style={style.boxout1}>
        <View
          style={{
            height: hp('10%'),
            backgroundColor: 'red',
            width: wp('100%'),
          }}>
          <HeaderMenu />
        </View>
        <View style={{marginTop: 120, marginBottom: 0}}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={{flexDirection: 'row', marginLeft: 20}}>
              <CardDiskon />
              <CardDiskon />
            </View>
          </ScrollView>
        </View>
        <View style={style.box}>
          <Pressable onPress={() => toggleModal()}>
            <View style={style.boxoout}>
              <Text>{Value}</Text>
              <Icon name="keyboard-arrow-down" size={25} color="grey" />
            </View>
          </Pressable>
        </View>
        <ScrollView>
          <View>
            <View style={style.boxmenu}>
              <Icon2 name="like1" size={20} color="#FF5353" />
              <Text style={style.text}> Recommended</Text>
            </View>
            <View>
              <FlatList
                data={data}
                numColumns={2}
                renderItem={({item}) => (
                  <View style={style.boxmenu2} style={{flexDirection: 'row'}}>
                    <CardMenu
                      handleNavigation={() => navigation.navigate('MenuDetail')}
                      imagemenu={item.imagemenu}
                      icon="like1"
                      menu={item.menu}
                      menu1={item.menu1}
                      price1={item.price1}
                      pricediscount={item.pricediscount}
                    />
                  </View>
                )}
              />
            </View>
            <View style={style.boxmenu}>
              <Icon2 name="like1" size={20} color="#FF5353" />
              <Text style={style.text}> Most favorite</Text>
            </View>
            <FlatList
              data={data2}
              numColumns={2}
              renderItem={({item}) => (
                <View style={style.boxmenu2} style={{flexDirection: 'row'}}>
                  <CardMenu
                    imagemenu={item.imagemenu}
                    icon="heart"
                    menu={item.menu}
                    menu1={item.menu1}
                    price1={item.price1}
                    pricediscount={item.pricediscount}
                  />
                </View>
              )}
            />
            <View style={style.boxmenu3}>
              <Text style={style.text}> Most favorite</Text>
            </View>
            <View style={{marginBottom: 280}}>
              <FlatList
                data={data}
                numColumns={2}
                renderItem={({item}) => (
                  <View style={style.boxmenu2} style={{flexDirection: 'row'}}>
                    <CardMenu
                      imagemenu={item.imagemenu}
                      menu={item.menu}
                      menu1={item.menu1}
                      price1={item.price1}
                      pricediscount={item.pricediscount}
                    />
                  </View>
                )}
              />
            </View>
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

export default Menu2;

const style = StyleSheet.create({
  boxout1: {flex: 1},
  boxoout: {
    backgroundColor: '#F5F7FA',
    width: wp('85%'),
    height: hp('6%'),
    flexDirection: 'row',
    borderRadius: 10,
    justifyContent: 'space-between',
    marginVertical: 20,
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  box: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  boxmenu: {
    marginHorizontal: 30,
    marginTop: 20,
    flexDirection: 'row',
  },
  text: {
    color: 'black',
    fontSize: 16,
    marginHorizontal: 5,
  },
  menu: {
    width: wp('95%'),

    flexDirection: 'row',
  },
  boxmenu1: {
    alignItems: 'center',
  },
  boxmenu2: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  boxmenu3: {
    marginHorizontal: 10,
    marginTop: 30,
    flexDirection: 'row',
  },
  view: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  textmodal: {
    lineHeight: 21,
    fontSize: 14,
    color: 'black',
    marginHorizontal: 10,
  },
  border: {
    height: 1,
    backgroundColor: '#D3D9FF',
    width: hp('100%'),
  },
});
