import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';
import Header from '../components/Header/HeaderProfile';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const Profile = props => {
  return (
    <View style={{flex: 1}}>
      <Header title="Profile" iconright="logout-variant" />
      <View style={styles2.contain}>
        <View style={styles2.main}>
          <Image
            source={require('../assets/image/gambar4.png')}
            style={styles2.image}
          />
          <Text style={styles2.text}>We’re all ears!</Text>
          <Text style={styles2.text1}>
            Thank you for your rating of our services
          </Text>
        </View>
        <View style={styles2.mid}>
          <TouchableOpacity
            onPress={() => props.navigation.navigate('ESProfile')}
            style={styles2.button}>
            <Text style={styles2.buttontxt}>Order Something</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default Profile;

const styles2 = StyleSheet.create({
  contain: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  main: {
    paddingVertical: 25,
    alignItems: 'center',
  },
  image: {
    width: wp('70%'),
    height: hp('23%'),
  },
  text: {
    color: '#0F120D',
    fontFamily: 'Poppins-Bold',
    fontSize: 16,
  },
  text1: {
    color: '#0F120D',
    fontFamily: 'Poppins-Reguler',
    fontSize: 16,
  },
  mid: {
    justifyContent: 'center',
    alignSelf: 'center',
  },
  button: {
    borderWidth: 1,
    borderColor: '#FF5353',
    borderRadius: 10,
    backgroundColor: '#FFFFFF',
    width: wp('90%'),
    height: hp('7%'),
    justifyContent: 'center',
  },
  buttontxt: {
    textAlign: 'center',
    fontSize: 16,
    fontFamily: 'Poppins-Reguler',
    color: '#FF5353',
  },
});
