import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import HeaderMerch from '../../components/Header/HeaderMerch';
import Star from 'react-native-vector-icons/MaterialCommunityIcons';
import Dropdown from '../../components/Dropdown/DropdownMerch';
import CardTransaksi from '../../components/card/CardTransaksi';
import CardOrder from '../../components/card/CardOrder';
import {launchImageLibrary} from 'react-native-image-picker';
import imagedefault from '../../assets/image/pictureheader.png';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const MerchantProfile = props => {
  const [Category, setCategory] = useState();

  const conditionaldropdown = select => {
    setCategory(select);
  };

  // useEffect(() => {
  //   console.log('INI Category', Category);
  // }, [Category]);

  const [image, setImage] = useState();
  const [rawImage, setRawImage] = useState();

  const options = {
    storageOptions: {
      skipBackup: false,
      path: 'images',
    },
  };

  function pickImage() {
    launchImageLibrary(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Eror: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = {
          uri: response.assets[0].uri,
          type: response.assets[0].type,
          name: response.assets[0].fileName,
        };

        setRawImage(source);
        setImage(response.assets[0].uri);
      }
    });
  }

  const conditional = false;

  return (
    <View style={{flex: 1, backgroundColor: '#FAF9FF'}}>
      <HeaderMerch title="Profile" iconleft="logout-variant" />
      <ScrollView style={style.container}>
        <ImageBackground
          style={style.bgimage}
          source={!image ? imagedefault : {uri: image}}>
          <View style={style.box}>
            <Text style={style.textHead}>Heavenly Taste</Text>
            <View style={style.textbot}>
              <Text style={style.text2}>hi@heavenly.taste</Text>
              <View style={style.line}></View>
              <Star
                name="star-circle"
                size={16}
                color={'#FFFFFF'}
                style={style.star}
              />
              <Text style={style.text3}>4.7</Text>
            </View>
            <TouchableOpacity
              onPress={() => pickImage()}
              style={style.touch}
              title="Change">
              <Text style={style.text4}>Change Header Photo</Text>
            </TouchableOpacity>
          </View>
        </ImageBackground>
        <Dropdown select={conditionaldropdown} label="Date" />
        {Category == 'Period' && (
          <>
            <Dropdown label="From" />
            <Dropdown label="To" />
          </>
        )}
        <View style={style.main}>
          <Text style={style.textsales}>Sales Summary</Text>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <TouchableOpacity style={style.cart}>
              <CardTransaksi title="TOTAL TRANSACTIONS" number="57" />
            </TouchableOpacity>
            <TouchableOpacity>
              <CardTransaksi title="PAID TRANSACTIONS" number="RP 10" />
            </TouchableOpacity>
          </ScrollView>
          <Text style={style.order}>Orders</Text>
          {conditional ? (
            <>
              <CardOrder
                title="Order ID"
                id="#927467375"
                tgl="Date"
                date="28/03/21"
                total="Total Order"
                numb="Rp. 156.500"
                status="Status"
                pay="Paid"
              />
              <View style={{marginVertical: 10}}></View>
              <CardOrder
                title="Order ID"
                id="#927467375"
                tgl="Date"
                date="28/03/21"
                total="Total Order"
                numb="Rp. 156.500"
                status="Status"
                pay="Unpaid"
              />
              <View style={{marginVertical: 10}}></View>
            </>
          ) : (
            <View style={style.noorders}>
              <Image
                source={require('../../assets/image/OrderShipped.png')}
                style={style.imgorders}
              />
              <Text style={style.textimg}>Uh-oh!</Text>
              <Text style={style.textimg}>
                No orders found. Try to change the timeframe, maybe?
              </Text>
            </View>
          )}
        </View>
      </ScrollView>
    </View>
  );
};

export default MerchantProfile;

const style = StyleSheet.create({
  container: {
    flex: 1,
  },
  bgimage: {
    height: hp('25%'),
    width: wp('100%'),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
  },
  textHead: {
    fontSize: 24,
    fontFamily: 'Poppins-Bold',
    color: 'white',
  },
  box: {
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
    flex: 1,
    height: hp('22%'),
    width: wp('100%'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  textbot: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 10,
    marginVertical: 10,
  },
  text2: {
    paddingRight: 15,
    fontSize: 16,
    color: 'white',
    fontFamily: 'Poppins-SemiBold',
  },
  text3: {
    fontSize: 16,
    color: 'white',
    fontFamily: 'Poppins-SemiBold',
  },
  line: {
    borderWidth: 1,
    borderColor: 'white',
    backgroundColor: 'white',
    height: 15,
    marginRight: 15,
    marginBottom: 3,
  },
  star: {
    marginRight: 5,
    marginBottom: 4.5,
  },
  touch: {
    backgroundColor: '#FF5353',
    borderColor: '#FF5353',
    borderRadius: 10,
    width: wp('60%'),
    height: hp('6%'),
    alignItems: 'center',
    justifyContent: 'center',
  },
  text4: {
    fontSize: 16,
    color: 'white',
    fontFamily: 'Poppins-SemiBold',
    textAlign: 'center',
  },
  main: {
    paddingHorizontal: 16,
    paddingVertical: 16,
    backgroundColor: '#FFFFFF',
  },
  textsales: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 16,
    color: '#000000',
    paddingBottom: 16,
  },
  cart: {
    marginRight: 20,
  },
  order: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 16,
    color: '#000000',
    marginTop: 30,
    paddingBottom: 16,
  },
  noorders: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  imgorders: {
    width: wp('65%'),
    height: hp('23%'),
    alignSelf: 'center',
    marginBottom: 10,
  },
  textimg: {
    fontSize: 16,
    fontFamily: 'Proxima-Nova-SBold',
    color: '#000000',
    textAlign: 'center',
    marginVertical: 5,
  },
});
