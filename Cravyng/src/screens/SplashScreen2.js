import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, Image, ImageBackground} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const Landingpage = props => {
  return (
    <View>
      <ImageBackground
        source={require('../assets/image/bgSplash.png')}
        style={{
          height: '100%',
          width: '100%',
        }}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.6)',
            justifyContent: 'center',
            height: '100%',
            alignItems: 'center',
          }}>
          <View style={style.vector}>
            <Image source={require('../assets/image/cravyng.png')} />
          </View>
        </View>
      </ImageBackground>
    </View>
  );
};

export default Landingpage;

const style = StyleSheet.create({
  vector: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
});
