import React, {useState} from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import Input from '../components/Input/Input';
import Back from '../components/Back/Back';
import Button from '../components/Button/Button';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useNavigation} from '@react-navigation/core';
const Login = props => {
  const [name, setName] = useState('');
  // const navigation = useNavigation();
  return (
    <View>
      <View>
        <Back name="Back" navigate="Menu" />
      </View>
      <View style={style.chiken}>
        <Image source={require('../assets/image/chiken.png')} />
      </View>
      <View style={style.hello}>
        <Text style={style.helloTxt}>Hello!</Text>
      </View>
      <View style={style.continue}>
        <Text style={style.continueTxt}>
          To continue, please enter your name
        </Text>
      </View>
      <View style={{marginVertical: '5%'}}>
        {/* <Input hint="Your Name" value={name} onTxt={setName()} /> */}
      </View>
      <View>
        <Button
          name="Continue"
          handleNavigation={() => props.navigation.navigate('Botnav')}
        />
      </View>
    </View>
  );
};
export default Login;

const style = StyleSheet.create({
  continueTxt: {
    color: '#868993',
    fontSize: 16,
  },
  continue: {
    marginLeft: '4%',
    marginTop: '1%',
  },
  hello: {
    marginLeft: '4%',
    marginTop: '5%',
  },
  helloTxt: {
    fontFamily: 'Poppins-Bold',
    fontSize: 31,
    color: '#313440',
  },
  chiken: {
    marginLeft: '4%',
    marginTop: '18%',
  },
});
