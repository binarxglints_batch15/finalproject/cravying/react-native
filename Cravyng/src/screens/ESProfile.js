import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Header from '../components/Header/HeaderProfile';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useNavigation} from '@react-navigation/core';

const Profile = props => {
  const navigation = useNavigation();
  return (
    <View style={{flex: 1}}>
      <Header title="Profile" iconright="logout-variant" />
      <View style={styles.contain}>
        <View style={styles.main}>
          <Text style={styles.maintxt}>Hai Rara!</Text>
          <Text style={styles.maintxt1}>
            Don’t let yourself turn into hangry person. Let’s check our menu and
            dig in!
          </Text>
        </View>
        <View style={styles.mid}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate('Menu')}>
            <Text style={styles.buttontxt}>Find Delicious Food</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default Profile;

const styles = StyleSheet.create({
  contain: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  main: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    paddingVertical: 30,
    paddingHorizontal: 20,
  },
  maintxt: {
    fontFamily: 'Poppins-Bold',
    fontSize: 24,
    color: '#313440',
  },
  maintxt1: {
    fontFamily: 'Poppins-Reguler',
    fontSize: 14,
    color: '#313440',
  },
  mid: {
    justifyContent: 'center',
    alignSelf: 'center',
  },
  button: {
    borderWidth: 1,
    borderColor: '#FF5353',
    borderRadius: 10,
    backgroundColor: '#FF5353',
    width: wp('90%'),
    height: hp('7%'),
    justifyContent: 'center',
  },
  buttontxt: {
    textAlign: 'center',
    fontSize: 16,
    fontFamily: 'Poppins-Reguler',
    color: '#FFFFFF',
  },
});
