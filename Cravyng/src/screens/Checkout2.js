import {NavigationContainer} from '@react-navigation/native';
import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useNavigation} from '@react-navigation/core';
export default function Checkout2() {
  const navigation = useNavigation();
  return (
    <View style={{backgroundColor: '#328E8E', flex: 1}}>
      <StatusBar backgroundColor="transparent" translucent={true} />
      <View style={{flex: 19}}>
        <View style={style.x}>
          <TouchableOpacity onPress={() => navigation.navigate('Menu')}>
            <Image source={require('../assets/image/Vector2.png')} />
          </TouchableOpacity>
        </View>
        <View style={style.checklist}>
          <Image source={require('../assets/image/checklist.png')} />
        </View>
        <View style={{alignItems: 'center', marginTop: '4%'}}>
          <Text style={style.order}>Order Placed</Text>
        </View>
        <View style={style.preparing}>
          <Text style={style.preparingTxt}>
            We're preparing your order right away
          </Text>
        </View>
        <View style={style.container}>
          <View style={style.body}>
            <Text style={style.txt}>Order ID</Text>
            <Text style={style.txt}>Value</Text>
          </View>
          <View style={style.body}>
            <Text style={style.txt}>Total Payment</Text>
            <Text style={style.txt}>Value</Text>
          </View>
        </View>
      </View>
      <View style={{flex: 3}}>
        <TouchableOpacity onPress={() => navigation.navigate('Order')}>
          <View style={style.btn}>
            <Text style={style.btnTxt}>Input New Order</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const style = StyleSheet.create({
  btnTxt: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: '#313440',
  },
  btn: {
    borderWidth: 1,
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    height: hp('8%'),
    width: wp('90%'),
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: '5%',
    marginVertical: 10,
    borderColor: '#313440',
  },
  container: {
    borderWidth: 1,
    marginTop: '5%',
    marginHorizontal: '5%',
    padding: '3%',
    borderRadius: 7,
    backgroundColor: 'rgba(49, 52, 64, 0.3)',
    borderColor: '#328E8E',
  },
  txt: {
    fontSize: 14,
    fontFamily: 'Proxima-Nova-Bold',
    color: '#FFFFFF',
  },
  body: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  preparingTxt: {
    color: '#FFFFFF',
    fontSize: 14,
  },
  preparing: {
    alignItems: 'center',
  },
  order: {
    color: '#FFFFFF',
    fontFamily: 'Poppins-Bold',
    fontSize: 21,
  },
  checklist: {
    alignItems: 'center',
    marginTop: '35%',
  },
  x: {
    alignItems: 'flex-end',
    marginRight: '4%',
    marginTop: '14%',
  },
});
